<?php

use Illuminate\Database\Seeder;
use \App\Models\Product\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Product::class, 5)->create();
    }
}
