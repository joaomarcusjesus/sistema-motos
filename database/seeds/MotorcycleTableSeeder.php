<?php

use Illuminate\Database\Seeder;
use App\Models\Motorcycle\Motorcycle;

class MotorcycleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Motorcycle::class, 5)->create();
    }
}
