<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            /**
             * Audit
             */
            ['name' => 'view_audits', 'details' => 'Auditorias [view]', 'guard' => 'dashboard'],

            /**
             * Permissions
             */
            ['name' => 'view_permissions', 'details' => 'Permissões [view]', 'guard' => 'dashboard'],
            ['name' => 'add_permissions', 'details' => 'Permissões [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_permissions', 'details' => 'Permissões [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_permissions', 'details' => 'Permissões [delete]', 'guard' => 'dashboard'],

            /**
             * Roles
             */
            ['name' => 'view_roles', 'details' => 'Grupos [view]', 'guard' => 'dashboard'],
            ['name' => 'add_roles', 'details' => 'Grupos [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_roles', 'details' => 'Grupos [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_roles', 'details' => 'Grupos [delete]', 'guard' => 'dashboard'],

            /**
             * Users
             */
            ['name' => 'view_users', 'details' => 'Usuários [view]', 'guard' => 'dashboard'],
            ['name' => 'add_users', 'details' => 'Usuários [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_users', 'details' => 'Usuários [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_users', 'details' => 'Usuários [delete]', 'guard' => 'dashboard'],

            /**
             * Analytics
             */
            ['name' => 'view_analytics', 'details' => 'Acessos [view]', 'guard' => 'dashboard'],

            /**
             * Messages
             */
            ['name' => 'view_messages', 'details' => 'Sac [view]', 'guard' => 'dashboard'],
            ['name' => 'delete_messages', 'details' => 'Sac [delete]', 'guard' => 'dashboard'],

            /**
             * Products
             */
            ['name' => 'view_products', 'details' => 'Produtos [view]', 'guard' => 'dashboard'],
            ['name' => 'add_products', 'details' => 'Produtos [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_products', 'details' => 'Produtos [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_products', 'details' => 'Produtos [delete]', 'guard' => 'dashboard'],

            /**
            * Products Submodules
            */
            ['name' => 'view_products_submodules', 'details' => 'Produtos Submodulos [view]', 'guard' => 'dashboard'],
            ['name' => 'add_products_submodules', 'details' => 'Produtos Submodulos [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_products_submodules', 'details' => 'Produtos Submodulos [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_products_submodules', 'details' => 'Produtos Submodulos [delete]', 'guard' => 'dashboard'],

            /**
             * Materials
             */
            ['name' => 'view_materials', 'details' => 'Materiais [view]', 'guard' => 'dashboard'],
            ['name' => 'add_materials', 'details' => 'Materiais [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_materials', 'details' => 'Materiais [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_materials', 'details' => 'Materiais [delete]', 'guard' => 'dashboard'],

            /**
             * Services
             */
            ['name' => 'view_services', 'details' => 'Servicos [view]', 'guard' => 'dashboard'],
            ['name' => 'add_services', 'details' => 'Servicos [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_services', 'details' => 'Servicos [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_services', 'details' => 'Servicos [delete]', 'guard' => 'dashboard'],

            /**
             * Motorcycles
             */
            ['name' => 'view_motorcycles', 'details' => 'Motos [view]', 'guard' => 'dashboard'],
            ['name' => 'add_motorcycles', 'details' => 'Motos [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_motorcycles', 'details' => 'Motos [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_motorcycles', 'details' => 'Motos [delete]', 'guard' => 'dashboard'],

            /**
             * Clients
             */
            ['name' => 'view_clients', 'details' => 'Clientes [view]', 'guard' => 'dashboard'],
            ['name' => 'add_clients', 'details' => 'Clientes [add]', 'guard' => 'dashboard'],
            ['name' => 'edit_clients', 'details' => 'Clientes [edit]', 'guard' => 'dashboard'],
            ['name' => 'delete_clients', 'details' => 'Clientes [delete]', 'guard' => 'dashboard'],
        ];

        foreach($permissions as $permission) {
            Permission::create([
                'name' => $permission['name'],
                'details' => $permission['details'],
                'guard_name' => $permission['guard']
            ]);
        }
    }
}
