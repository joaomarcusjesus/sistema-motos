<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aulicio = User::create([
            'first_name' => 'Aulicio',
            'last_name' => '',
            'phone' => '',
            'email' => 'auliciolima@yahoo.com.br',
            'password' => bcrypt('mudarsenha123'),
            'email_verified_at' => now()
        ]);

        $aulicio->assignRole('root');

        $habel = User::create([
            'first_name' => 'habel',
            'last_name' => '',
            'phone' => '',
            'email' => 'habel@yahoo.com.br',
            'password' => bcrypt('mudarsenha123'),
            'email_verified_at' => now()
        ]);

        $habel->assignRole('gestor');

        $bruno = User::create([
            'first_name' => 'bruno',
            'last_name' => '',
            'phone' => '',
            'email' => 'bruno@yahoo.com.br',
            'password' => bcrypt('mudarsenha123'),
            'email_verified_at' => now()
        ]);

        $bruno->assignRole('funcionario');
    }
}
