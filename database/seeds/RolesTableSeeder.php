<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Role;
use App\Models\Auth\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create Root role
         */
        $root = Role::create([
            'name' => 'root',
            'details' => 'Root',
            'guard_name' => 'dashboard'
        ]);

        $root->syncPermissions(Permission::all());

        /**
         * Create Gestor role
         */
        $gestor = Role::create([
            'name' => 'gestor',
            'details' => 'Gestor',
            'guard_name' => 'dashboard'
        ]);

        $gestor->syncPermissions(Permission::whereNotIn('name', [
            'view_permissions', 'add_permissions', 'edit_permissions', 'delete_permissions',
            'view_settings', 'add_settings', 'edit_settings', 'delete_settings'
        ])->get());

        /**
         * Create Funcionário role
         */
        $funcionario = Role::create([
            'name' => 'funcionario',
            'details' => 'Funcionario',
            'guard_name' => 'dashboard'
        ]);

        $funcionario->syncPermissions(Permission::whereNotIn('name', [
            'view_permissions', 'add_permissions', 'edit_permissions', 'delete_permissions',
            'view_settings', 'add_settings', 'edit_settings', 'delete_settings',
            'view_users', 'add_users', 'edit_users', 'delete_users'
        ])->get());
    }
}
