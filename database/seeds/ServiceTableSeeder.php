<?php

use Illuminate\Database\Seeder;
use App\Models\Service\Service;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Service::class, 5)->create();
    }
}
