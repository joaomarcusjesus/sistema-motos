<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Service\Service;
use App\Models\Product\Product;
use App\Models\Client\Client;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->word,
        'body' => $faker->text,
        'product_id' => factory(Product::class),
        'client_id' => factory(Client::class),
    ];
});
