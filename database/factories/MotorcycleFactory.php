<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Motorcycle\Motorcycle;
use \App\Models\Client\Client;

$factory->define(Motorcycle::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->word,
        //'brand' => $faker->word,
        'year' => $faker->year,
        'model' => $faker->word,
        'board' => $faker->word,
        'color' => $faker->colorName,
        'body' => $faker->text,
        'displacement' => $faker->randomDigit,
        'client_id' => factory(Client::class),
    ];
});
