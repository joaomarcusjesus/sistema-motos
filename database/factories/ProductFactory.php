<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->word,
        'subtitle' => $faker->word,
        'body' => $faker->text,
        'price' => '5.00',
        'stock' => $faker->randomDigit,
        'category_id' => factory(ProductCategory::class),
    ];
});
