<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Models\Product\ProductCategory;
use App\Model;
use Faker\Generator as Faker;

$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->word,
    ];
});
