<?php

namespace App\Http\Controllers\Api\MediaLibrary;

use App\Http\Controllers\Api\BaseController;
use Illuminate\Http\Request;
use App\Models\Media\MediaLibrary;

class MediaLibraryController extends BaseController
{
    private $media;

    /**
     * Constructor
     */
    public function __construct(MediaLibrary $media)
    {
        // Dependency Injection
        $this->media = $media;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // Fetch all results
            $images = $this->media->latest()->get()
                                  ->map(function ($item) {
                                      return [
                                          'thumb' => $item->thumb,
                                          'url'   => $item->url,
                                          'id'    => "file-{$item->id}",
                                          'title' => $item->name
                                      ];
                                  })->toArray();

            // Return success response
            return response()->json($images, 200);
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if ($request->ajax()) {
            foreach ($request->file('file') as $key => $image) {
                // Upload file if send
                if ($result = $this->media->create($request->all())) {
                    // Upload file if send
                    storeMedia($result, $image, $image->getClientOriginalName(), 'media_library');
                    // Return success response
                    return response()->json(['file' => ['url' => $result->url, 'id' => $result->id]], 200);
                } else {
                    // Return error response
                    return response()->json(['success' => false, 'error' => 'Falha ao salvar arquivo.'], 400);
                }
            }
        } else {
            abort(404);
        }
    }
}
