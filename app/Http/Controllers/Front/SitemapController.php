<?php

namespace App\Http\Controllers\Front;

use Carbon\Carbon;

class SitemapController extends BaseController
{
    /**
     * Return sitemap
     */
    public function index()
    {
        $sitemap = app()->make('sitemap');
        $sitemap->setCache('laravel.sitemap', now()->addSeconds(3600));

        if (!$sitemap->isCached()) {
            $sitemap->add(route('home.index'), '2018-08-16T20:10:00+02:00', '1.0', 'daily');
        }

        return $sitemap->render('xml');
    }
}
