<?php

namespace App\Http\Controllers\Front;

use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class HomeController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Return view home
     */
    public function index()
    {
        // Set meta tags
        $this->seo()->setDescription('');

        // Set opengraph tags
        $this->seo()->opengraph()
                    ->setDescription('')
                    ->addImage(asset('/assets/images/facebook.jpg'));

        // Return view
        return view('front.home.index');
    }
}
