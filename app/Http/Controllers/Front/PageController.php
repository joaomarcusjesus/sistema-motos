<?php

namespace App\Http\Controllers\Front;

use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class PageController extends BaseController
{
    use SEOToolsTrait;

    /**
     * Return view contact
     */
    public function contact()
    {
        // Set meta tags
        $this->seo()->setTitle('Contato')
                    ->setDescription('');

        // Set opengraph tags
        $this->seo()->opengraph()
                    ->setDescription('')
                    ->addImage(asset('/assets/images/facebook.jpg'));

        // Return view
        return view('front.pages.contact.index');
    }
}
