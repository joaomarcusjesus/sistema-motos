<?php
namespace App\Http\Controllers\Admin\Motorcycles;

use App\Models\Client\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Motorcycle\Motorcycle;

class MotorcycleController extends Controller
{
    use SEOToolsTrait;

    /**
     * @var \App\Models\Motorcycle\Motorcycle
     */
    private $motorcycles;

    /**
     * @var \App\Models\Client\Client
     */
    private $clients;

    /**
     * Constructor
     */
    public function __construct(
        Motorcycle $motorcycles,
        Client $clients
    ) {
        // Middlewares
        $this->middleware('permission:view_motorcycles', ['only' => ['index']]);
        $this->middleware('permission:add_motorcycles', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_motorcycles', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_motorcycles', ['only' => ['destroy']]);

        // Dependency Injection
        $this->motorcycles = $motorcycles;
        $this->clients = $clients;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // Start query
        $query = $this->motorcycles->orderBy('title', 'ASC');

        // Filter by name param
        if ($request->filled('nome')):
            $title = $request->get('nome');
            $query->where('title', 'like', "%{$title}%");
        endif;

        // Filter by model param
        if ($request->filled('modelo')):
            $model = $request->get('modelo');
            $query->where('model', 'like', "%{$model}%");
        endif;

        // Filter by board param
        if ($request->filled('placa')):
            $board = $request->get('placa');
            $query->where('board', 'like', "%{$board}%");
        endif;

        // Filter by client param
        if ($request->filled('cliente')):
            $clientId = $request->get('cliente');
            $query->whereHas('client', function ($q) use ($clientId) {
                $q->where('client_id', $clientId);
            });
        endif;

        // Fetch all results
        $results = $query->paginate(10)
            ->appends($request->except('page'));

        // Fetch clients active
        $clients = $this->clients->actived()
            ->get()
            ->pluck('full_name', 'id');

        // Set meta tags
        $this->seo()->setTitle('Motos');

        // Return view
        return view('admin.motorcycles.index', compact('results', 'clients'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Fetch clients active
        $clients = $this->clients->actived()
            ->get()
            ->pluck('full_name', 'id');

        // Set meta tags
        $this->seo()->setTitle('Nova Moto');

        // Return view
        return view('admin.motorcycles.create', compact('clients'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => 'required|max:255',
            'year' => 'nullable|max:255',
            'model' => 'nullable|max:255',
            'body' => 'nullable',
            'client_id' => 'required',
            'color' => 'nullable|max:255',
            'type' => 'nullable|max:255',
            'displacement' => 'nullable|max:255',
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao adicionar Moto.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Create result
        $this->motorcycles->create($request->all());

        // Success message
        flash('Moto criada com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.motorcycles.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // Fetch clients active
        $clients = $this->clients->actived()
            ->get()
            ->pluck('full_name', 'id');

        // Fetch result by id
        $result = $this->motorcycles->findOrFail($id);

        // Set meta tags
        $this->seo()->setTitle('Editar Moto');

        // Return view
        return view('admin.motorcycles.edit', compact('result', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => "required|max:255",
            'year' => 'nullable|max:255',
            'model' => 'nullable|max:255',
            'body' => 'nullable',
            'client_id' => 'required',
            'color' => 'nullable|max:255',
            'type' => 'nullable|max:255',
            'displacement' => 'nullable|max:255',
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao atualizar moto.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Fetch result
        $result = $this->motorcycles->findOrFail($id);

        // Fill data and save
        $result->fill($request->all())->save();

        // Success message
        flash('Moto atualizada com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.motorcycles.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()):
            // If result exist
            if ($result = $this->motorcycles->find($id)):
                // Remove result
                $result->delete();

                // Return success response
                return response()->json(['success' => true, 'message' => 'Moto removida com sucesso.'], 200);
            else:
                // Return error response
                return response()->json(['success' => false, 'message' => 'Moto não encontrada.'], 400);
            endif;
        endif;

        // Error message
        flash('Falha ao remover moto.')->error();

        // Redirect to back page
        return redirect()->back();
    }

}
