<?php
namespace App\Http\Controllers\Admin\Materials;

use App\Models\Client\Client;
use App\Models\Product\Product;
use App\Models\Motorcycle\Motorcycle;
use App\Models\Service\Service;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class MaterialController extends Controller
{
    use SEOToolsTrait;

    /**
     * @var \App\Models\Motorcycle\Motorcycle
     */
    private $motorcycles;

    /**
     * @var \App\Models\Client\Client
     */
    private $clients;

    /**
     * @var \App\Models\Product\Product
     */
    private $products;

    /**
     * @var \App\Models\Service\Service
     */
    private $services;

    /**
     * Constructor
     */
    public function __construct(
        Service $service,
        Motorcycle $motorcycle,
        Product $product,
        Client $client
    ) {
        // Middlewares
        $this->middleware('permission:view_materials', ['only' => ['index']]);
        $this->middleware('permission:add_materials', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_materials', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_materials', ['only' => ['destroy']]);

        // Dependency Injection
        $this->services = $service;
        $this->motorcycles = $motorcycle;
        $this->products = $product;
        $this->clients = $client;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // Start query
        $query = $this->services->orderBy('title', 'ASC');

        // Filter by client param
        if ($request->filled('cliente')):
            $clientId = $request->get('cliente');
            $query->whereHas('motorcycle', function ($q) use ($clientId) {
                $q->where('client_id', $clientId);
            });
        endif;

        // Filter by data param
        if ($request->filled('data')):
            $data = Carbon::createFromFormat('d/m/Y', $request->get('data'))->format('Y-m-d');
            $query->whereDate('created_at', $data);
        endif;

        // Filter by product_id param
        if ($request->filled('produto')):
            $productId = $request->get('produto');
            $query->whereHas('product', function ($q) use ($productId) {
               $q->where('id', $productId);
            });
        endif;

        // Fetch all results
        $results = $query->paginate(10)
            ->appends($request->except('page'));

        // Fetch products actived
        $products = $this->products->actived()
            ->pluck('title', 'id');

        // Fetch clients active
        $clients = $this->clients->actived()
            ->get()
            ->pluck('full_name', 'id');

        // Set meta tags
        $this->seo()->setTitle('Materiais');

        // Return view
        return view('admin.materials.index', compact('results', 'products', 'clients'));
    }

}
