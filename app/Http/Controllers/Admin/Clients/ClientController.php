<?php
namespace App\Http\Controllers\Admin\Clients;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Client\Client;

class ClientController extends Controller
{
    use SEOToolsTrait;

    /**
     * @var \App\Models\Client\Client
     */
    private $clients;

    /**
     * Constructor
     */
    public function __construct(Client $clients)
    {
        // Middlewares
        $this->middleware('permission:view_clients', ['only' => ['index']]);
        $this->middleware('permission:add_clients', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_clients', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_clients', ['only' => ['destroy']]);

        // Dependency Injection
        $this->clients = $clients;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // Start query
        $query = $this->clients->orderBy('first_name', 'ASC');

        // Filter by nome param
        if ($request->filled('nome')):
            $name = $request->get('nome');
            $query->where('first_name', 'like', "%{$name}%");
            $query->orWhere('last_name', 'like', "%{$name}%");
        endif;

        // Filter by email param
        if ($request->filled('email')):
            $query->where('email', $request->get('email'));
        endif;

        // Fetch all results
        $results = $query->paginate(10)
            ->appends($request->except('page'));

        // Set meta tags
        $this->seo()->setTitle('Clientes');

        // Return view
        return view('admin.clients.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Set meta tags
        $this->seo()->setTitle('Novo Cliente');

        // Return view
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate
        $validate = validator($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => 'required|max:255|unique:clients',
            'address' => 'nullable|max:255',
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao adicionar Cliente.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Create result
        $this->clients->create($request->all());

        // Success message
        flash('Cliente criado com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.clients.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // Fetch result by id
        $result = $this->clients->findOrFail($id);

        // Set meta tags
        $this->seo()->setTitle('Editar Cliente');

        // Return view
        return view('admin.clients.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validate
        $validate = validator($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required|max:255',
            'email' => "required|max:255|unique:clients,email,{$id}",
            'address' => 'nullable|max:255',
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao atualizar cliente.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Fetch result
        $result = $this->clients->findOrFail($id);

        // Fill data and save
        $result->fill($request->all())->save();

        // Success message
        flash('Cliente atualizado com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.clients.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()):
            // If result exist
            if ($result = $this->clients->find($id)):
                // Remove result
                $result->delete();

                // Return success response
                return response()->json(['success' => true, 'message' => 'Cliente removido com sucesso.'], 200);
            else:
                // Return error response
                return response()->json(['success' => false, 'message' => 'Cliente não encontrado.'], 400);
            endif;
        endif;

        // Error message
        flash('Falha ao remover cliente.')->error();

        // Redirect to back page
        return redirect()->back();
    }
}
