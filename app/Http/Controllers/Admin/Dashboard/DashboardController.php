<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Admin\BaseController;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class DashboardController extends BaseController
{
    use SEOToolsTrait;

    private $messages;

    /**
     * Return index
     **/
    public function index()
    {
        $visitorsData = collect();

        // Set meta tags
        $this->seo()->setTitle('Dashboard');

        // Return view
        return view('admin.dashboard.index', compact('visitorsData'));
    }
}
