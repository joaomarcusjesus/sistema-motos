<?php

namespace App\Http\Controllers\Admin\Session;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends BaseController
{
    use SendsPasswordResetEmails;

    public function broker()
    {
        return Password::broker();
    }

    public function showLinkRequestForm()
    {
        return view('admin.passwords.forgot');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink($this->credentials($request));

        if ($response == Password::RESET_LINK_SENT) {
            // Success message
            flash("E-mail enviado com sucesso para {$request->email}.")->success();
            // Return success response
            return $this->sendResetLinkResponse($request, $response);
        } else {
            // Warning message
            flash('Ocorreu um erro ao enviar email de recuperação.')->warning();

            // Return failed response
            return $this->sendResetLinkFailedResponse($request, $response);
        }
    }
}
