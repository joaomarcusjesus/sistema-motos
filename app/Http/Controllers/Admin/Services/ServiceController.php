<?php
namespace App\Http\Controllers\Admin\Services;

use App\Models\Client\Client;
use App\Models\Motorcycle\Motorcycle;
use App\Models\Product\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Service\Service;

class ServiceController extends Controller
{
    use SEOToolsTrait;

    /**
     * @var \App\Models\Service\Service
     */
    private $services;

    /**
     * @var \App\Models\Motorcycle\Motorcycle
     */
    private $motorcycles;

    /**
     * @var \App\Models\Product\Product
     */
    private $products;

    /**
     * @var \App\Models\Client\Client
     */
    private $clients;

    /**
     * Constructor
     */
    public function __construct(
        Service $services,
        Motorcycle $motorcycles,
        Product $products,
        Client $clients
    )
    {
        // Middlewares
        $this->middleware('permission:view_services', ['only' => ['index']]);
        $this->middleware('permission:add_services', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_services', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_services', ['only' => ['destroy']]);

        // Dependency Injection
        $this->services = $services;
        $this->motorcycles = $motorcycles;
        $this->products = $products;
        $this->clients = $clients;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // Start query
        $query = $this->services->orderBy('title', 'ASC');

        // Filter by nome param
        if ($request->filled('nome')):
            $title = $request->get('nome');
            $query->where('title', 'like', "%{$title}%");
        endif;

        // Filter by client_id param
        if ($request->filled('cliente')):
            $client_id = $request->get('cliente');
            $query->where('client_id', $client_id);
        endif;

        // Filter by data param
        if ($request->filled('data')):
            $data = Carbon::createFromFormat('d/m/Y', $request->get('data'))->format('Y-m-d');
            $query->whereDate('created_at', $data);
        endif;

        // Fetch all results
        $results = $query->paginate(10);

        // Fetch clients active
        $clients = $this->clients->actived()
            ->get()
            ->pluck('full_name', 'id');

        // Set meta tags
        $this->seo()->setTitle('Serviços');

        // Return view
        return view('admin.services.index', compact('results', 'clients'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Fetch products
        $products = $this->products->actived()
            ->pluck('title', 'id');

        // Fetch clients
        $clients = $this->clients->actived()
            ->get()
            ->pluck('full_name', 'id');

        // Set meta tags
        $this->seo()->setTitle('Novo Serviço');

        // Return view
        return view('admin.services.create', compact('clients', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => 'required|max:255|unique:services',
            'body' => 'nullable',
            'product_id' => 'required',
            'client_id' => 'required',
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao adicionar Serviço.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Create result
        $this->services->create($request->all());

        // Success message
        flash('Serviço criado com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.services.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // Fetch result by id
        $result = $this->services->findOrFail($id);

        // Fetch products
        $products = $this->products->actived()
            ->pluck('title', 'id');

        // Fetch clients
        $clients = $this->clients->actived()
            ->get()
            ->pluck('full_name', 'id');

        // Set meta tags
        $this->seo()->setTitle('Editar Serviço');

        // Return view
        return view('admin.services.edit', compact('result', 'products', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => 'required|max:255|unique:services,title,' . $id,
            'body' => 'nullable',
            'product_id' => 'required',
            'client_id' => 'required',
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao atualizar servico.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Fetch result
        $result = $this->services->findOrFail($id);

        // Fill data and save
        $result->fill($request->all())->save();

        // Success message
        flash('Serviço atualizado com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.services.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()):
            // If result exist
            if ($result = $this->services->find($id)):
                // Remove result
                $result->delete();

                // Return success response
                return response()->json(['success' => true, 'message' => 'Serviço removida com sucesso.'], 200);
            else:
                // Return error response
                return response()->json(['success' => false, 'message' => 'Serviço não encontrada.'], 400);
            endif;
        endif;

        // Error message
        flash('Falha ao remover servico.')->error();

        // Redirect to back page
        return redirect()->back();
    }

}
