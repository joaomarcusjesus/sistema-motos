<?php
namespace App\Http\Controllers\Admin\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Product\ProductCategory;

class ProductCategoryController extends Controller
{
    use SEOToolsTrait;

    /**
     * @var \App\Models\Product\ProductCategory
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct(ProductCategory $categories)
    {
        // Middlewares
        $this->middleware('permission:view_products_submodules', ['only' => ['index']]);
        $this->middleware('permission:add_products_submodules', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_products_submodules', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_products_submodules', ['only' => ['destroy']]);

        // Dependency Injection
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // Start query
        $query = $this->categories->orderBy('title', 'ASC');

        // Filter by nome param
        if ($request->filled('nome')):
            $title = $request->get('nome');
            $query->where('title', 'like', "%{$title}%");
        endif;

        // Fetch all results
        $results = $query->paginate(10)
            ->appends($request->except('page'));

        // Set meta tags
        $this->seo()->setTitle('Categorias');

        // Return view
        return view('admin.products_categories.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Set meta tags
        $this->seo()->setTitle('Nova Categoria');

        // Return view
        return view('admin.products_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => 'required|max:255|unique:products_categories',
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao adicionar Categoria.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Create result
        $this->categories->create($request->all());

        // Success message
        flash('Categoria criada com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.products_categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // Fetch result by id
        $result = $this->categories->findOrFail($id);

        // Set meta tags
        $this->seo()->setTitle('Editar Categoria');

        // Return view
        return view('admin.products_categories.edit', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => "required|max:255|unique:products_categories,title,{$id}",
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao atualizar categoria.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Fetch result
        $result = $this->categories->findOrFail($id);

        // Fill data and save
        $result->fill($request->all())->save();

        // Success message
        flash('Categoria atualizada com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.products_categories.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()):
            // If result exist
            if ($result = $this->categories->find($id)):
                // Remove result
                $result->delete();

                // Return success response
                return response()->json(['success' => true, 'message' => 'Categoria removida com sucesso.'], 200);
            else:
                // Return error response
                return response()->json(['success' => false, 'message' => 'Categoria não encontrada.'], 400);
            endif;
        endif;

        // Error message
        flash('Falha ao remover categoria.')->error();

        // Redirect to back page
        return redirect()->back();
    }
}
