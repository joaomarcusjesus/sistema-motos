<?php
namespace App\Http\Controllers\Admin\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;

class ProductController extends Controller
{
    use SEOToolsTrait;

    /**
     * @var \App\Models\Product\Product
     */
    private $products;

    /**
     * @var \App\Models\Product\ProductCategory
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct(
        Product $products,
        ProductCategory $categories
    ) {
        // Middlewares
        $this->middleware('permission:view_products', ['only' => ['index']]);
        $this->middleware('permission:add_products', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_products', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_products', ['only' => ['destroy']]);

        // Dependency Injection
        $this->products = $products;
        $this->categories = $categories;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        // Start query
        $query = $this->products->orderBy('title', 'ASC');

        // Filter by title param
        if ($request->filled('nome')):
            $title = $request->get('nome');
            $query->where('title', 'like', "%{$title}%");
        endif;

/*        // Filter by category_id param
        if ($request->filled('categoria')):
            $category = $request->get('categoria');
            $query->where('category_id', $category);
        endif;
*/

        // Fetch all results
        $results = $query->paginate(10);

/*        // Fetch categories actived
        $categories = $this->categories->actived()
            ->pluck('title', 'id');
*/

        // Set meta tags
        $this->seo()->setTitle('Produtos');

        // Return view
        return view('admin.products.index', compact('results'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Fetch categories actived
        $categories = $this->categories->actived()
            ->pluck('title', 'id');

        // Set meta tags
        $this->seo()->setTitle('Novo Produto');

        // Return view
        return view('admin.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => 'required|max:255|unique:products',
            'subtitle' => 'required|max:255',
            'body' => 'nullable',
            'price' => 'required',
            'price_promotional' => 'nullable',
            'stock' => 'required|integer',
            'category_id' => 'nullable'
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao adicionar Produto.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Create result
        $this->products->create($request->all());

        // Success message
        flash('Produto criado com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.products.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // Fetch categories actived
        $categories = $this->categories->actived()
            ->pluck('title', 'id');

        // Fetch result by id
        $result = $this->products->findOrFail($id);

        // Set meta tags
        $this->seo()->setTitle('Editar Produto');

        // Return view
        return view('admin.products.edit', compact('result', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        // Validate
        $validate = validator($request->all(), [
            'title' => "required|max:255|unique:products,title,{$id}",
            'subtitle' => 'required|max:255',
            'body' => 'nullable',
            'price' => 'required',
            'price_promotional' => 'nullable',
            'stock' => 'required|integer',
            'category_id' => 'nullable'
        ]);

        // If fails validate
        if ($validate->fails()):
            // Warning message
            flash('Falha ao atualizar produto.')->warning();

            // Redirect same page with errors messages
            return redirect()->back()->withInput()->withErrors($validate->getMessageBag());
        endif;

        // Fetch result
        $result = $this->products->findOrFail($id);

        // Fill data and save
        $result->fill($request->all())->save();

        // Success message
        flash('Produto atualizado com sucesso.')->success();

        // Redirect to list
        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()):
            // If result exist
            if ($result = $this->products->find($id)):
                // Remove result
                $result->delete();

                // Return success response
                return response()->json(['success' => true, 'message' => 'Produto removido com sucesso.'], 200);
            else:
                // Return error response
                return response()->json(['success' => false, 'message' => 'Produto não encontrado.'], 400);
            endif;
        endif;

        // Error message
        flash('Falha ao remover produto.')->error();

        // Redirect to back page
        return redirect()->back();
    }

}
