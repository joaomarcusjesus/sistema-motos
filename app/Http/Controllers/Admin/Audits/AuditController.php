<?php

namespace App\Http\Controllers\Admin\Audits;

use App\Http\Controllers\Admin\BaseController;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use App\Models\Audit\Audit;
use App\Models\Auth\User;
use Carbon\Carbon;

class AuditController extends BaseController
{
    use SEOToolsTrait;

    private $audits;
    private $users;

    /**
     * Construct
     */
    public function __construct(Audit $audits, User $users)
    {
        // Middlewares
        $this->middleware('permission:view_audits', ['only' => ['index', 'show']]);

        // Dependency Injection
        $this->audits = $audits;
        $this->users = $users;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        // Fetch all users
        $users = $this->users->isRoot(auth()->user())
                             ->orderBy('first_name', 'ASC')
                             ->get()
                             ->pluck('full_name', 'id');

        // Fetch all results
        $results = $this->audits->wherePeriod($request->get('data'))
                                ->whereUser($request->get('usuario'))
                                ->whereType($request->get('tipo'))
                                ->whereAction($request->get('acao'))
                                ->latest()
                                ->paginate(10)
                                ->appends($request->except('page'));

        // Set meta tags
        $this->seo()->setTitle('Auditoria');

        // Return view
        return view('admin.audits.index', compact('results', 'users'));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        // Fetch result by id
        $result = $this->audits->findOrFail($id);

        // Set meta tags
        $this->seo()->setTitle('Log');

        // Return view
        return view('admin.audits.show', compact('result'));
    }
}
