<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect()->route($this->getRouteByGuard($guard));
        }

        return $next($request);
    }

    private function getRouteByGuard($guard)
    {
        $route = 'home.index';

        switch ($guard) {
            case 'dashboard':
            default:
                $route = 'admin.dashboard.index';
                break;
        }

        return $route;
    }
}
