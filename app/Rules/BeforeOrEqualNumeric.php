<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BeforeOrEqualNumeric implements Rule
{
    public $compare;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($compare)
    {
        $this->compare = $compare;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = (int)trim(unmaskInput($value));
        $compare = (int)trim(unmaskInput($this->compare));

        return $value <= $compare;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.numeric.before_or_equal', ['compare' => $this->compare]);
    }
}
