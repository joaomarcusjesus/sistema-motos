<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UniqueByTaxonomy implements Rule
{
    public $taxonomy;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($taxonomy)
    {
        $this->taxonomy = $taxonomy;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return DB::table('taxonomies')->where('title', $value)->where('taxonomy', $this->taxonomy)->doesntExist();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.taxonomy.unique');
    }
}
