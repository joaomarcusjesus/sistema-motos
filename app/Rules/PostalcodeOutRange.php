<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PostalcodeOutRange implements Rule
{
    public $table;
    public $fields;
    public $compare;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($table, $fields, $compare)
    {
        $this->table = $table;
        $this->fields = $fields;
        $this->compare = $compare;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = trim(unmaskInput($value));
        $compare = trim(unmaskInput($this->compare));

        $firstCompareField = $this->fields[0];
        $lastCompareField = $this->fields[1];

        return DB::table($this->table)->where($firstCompareField, '>', $value)
                                      ->where($lastCompareField, '<', $compare)
                                      ->doesntExist();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.postalcode.out_range');
    }
}
