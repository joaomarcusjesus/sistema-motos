<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PostalcodeInRange implements Rule
{
    public $table;
    public $fields;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($table, $fields)
    {
        $this->table = $table;
        $this->fields = $fields;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = trim(unmaskInput($value));

        $firstCompareField = $this->fields[0];
        $lastCompareField = $this->fields[1];

        return DB::table($this->table)->where($firstCompareField, '<=', $value)
                                      ->where($lastCompareField, '>=', $value)
                                      ->doesntExist();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.postalcode.in_range');
    }
}
