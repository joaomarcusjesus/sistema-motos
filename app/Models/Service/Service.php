<?php
namespace App\Models\Service;

use App\Models\Client\Client;
use App\Models\Product\Product;
use App\Traits\User\HasAuthor;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Service extends Model implements AuditableInterface
{
    use AuditableTrait;
    use Sluggable;
    use HasAuthor;

    protected $fillable = [
        'slug',
        'title',
        'body',
        'active',
        'product_id',
        'client_id',
        'user_id',
    ];

    protected $auditInclude = [
        'slug',
        'title',
        'body',
        'active',
        'product_id',
        'client_id',
        'user_id',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function scopeActived($query)
    {
        return $query->where('active', true);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
