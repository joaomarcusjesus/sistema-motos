<?php
namespace App\Models\Client;

use App\Traits\User\HasAuthor;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Client extends Model implements AuditableInterface
{
    use AuditableTrait;
    use HasAuthor;

    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'address',
        'active',
        'user_id',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    protected $auditInclude = [
        'first_name',
        'last_name',
        'phone',
        'email',
        'active',
        'address'
    ];

    public function scopeActived($query)
    {
        return $query->where('active', true);
    }

    public function setEmailAttribute($input): void
    {
        if ($input) {
            $this->attributes['email'] = mb_strtolower($input, 'UTF-8');
        }
    }

    public function setPhoneAttribute($input): void
    {
        if ($input) {
            $this->attributes['phone'] = unmaskInput($input);
        }
    }

    public function getFullNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
