<?php

namespace App\Models\Auth;

use Spatie\Permission\Models\Permission as SpatiePermission;

use App\Presenters\Permission\UrlPresenter;

class Permission extends SpatiePermission
{
    protected $fillable = [
        'name',
        'details',
        'guard_name'
    ];

    /**
     * Accesors
     */
    public function getUrlAttribute()
    {
        return new UrlPresenter($this);
    }
}
