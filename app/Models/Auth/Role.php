<?php

namespace App\Models\Auth;

use Spatie\Permission\Models\Role as SpatieRole;

use App\Presenters\Role\UrlPresenter;

class Role extends SpatieRole
{
    protected $fillable = [
        'name',
        'details',
        'guard_name'
    ];

    /**
     * Scopes
     */
    public function scopeByUser($query, $currentUser)
    {
        $query->orderBy('id', 'ASC');

        // If user in role root
        if(!$currentUser->hasRole('root')) {
            if ($currentUser->hasRole('gestor')) {
                $query->where('name', '!=', 'root');
            // If user other roles
            } else {
                $query->whereNotIn('name', ['root', 'gestor']);
            }
        }

        return $query;
    }

    /**
     * Accesors
     */
    public function getUrlAttribute()
    {
        return new UrlPresenter($this);
    }
}
