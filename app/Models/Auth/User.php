<?php
namespace App\Models\Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\User\VerifyEmail as VerifyEmailNotification;
use App\Notifications\User\ResetPassword as ResetPasswordNotification;
use Spatie\Permission\Traits\HasRoles;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Image\Manipulations;
use App\Traits\Actived\ActiveTrait;

class User extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use Notifiable;
    use HasRoles;
    use HasMediaTrait;
    use ActiveTrait;

    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'phone',
        'email',
        'password',
        'last_login_ip',
        'last_login_at',
        'receive_messages',
        'active'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime',
        'receive_messages' => 'boolean',
        'active' => 'boolean'
    ];

    protected $attributes = [
        'receive_messages' => false
    ];

    public function scopeWherePassport($query, $identifier)
    {
        return $query->orWhere('email', $identifier);
    }

    public function scopeIsRoot($query, $user)
    {
        if(!$user->hasRole('root')) {
            return $query->whereHas('roles', function($q) {
                $q->where('name', '<>', 'root');
            });
        }
    }

    public function scopeAcceptMessages($query, $status = true) {
        return $query->where('receive_messages', $status);
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('avatar')
             ->fit(Manipulations::FIT_CROP, 360, 360)
             ->quality(85)
             ->nonQueued();
    }

    public function setUsernameAttribute($input)
    {
        if ($input)
            $this->attributes['username'] = mb_strtolower($input, 'UTF-8');
    }

    public function setEmailAttribute($input)
    {
        if ($input)
            $this->attributes['email'] = mb_strtolower($input, 'UTF-8');
    }

    public function setPhoneAttribute($input)
    {
        if ($input)
            $this->attributes['phone'] = unmaskInput($input);
    }

    public function getLastLoginAttribute()
    {
        return ($this->last_login_at) ? $this->last_login_at->diffForHumans() : 'Não efetuou login';
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getPhotoAttribute()
    {
        $image = $this->getFirstMedia('user');
        return isset($image) ? $image->getUrl('avatar') : asset('/brzadmin/images/avatar.svg');
    }
}
