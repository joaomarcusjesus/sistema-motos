<?php
namespace App\Models\Motorcycle;

use App\Models\Client\Client;
use App\Traits\User\HasAuthor;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Motorcycle extends Model implements AuditableInterface
{
    use AuditableTrait;
    //use Sluggable;
    use HasAuthor;

    protected $fillable = [
        //'slug',
        'title',
        'year',
        'model',
        'board',
        'body',
        'active',
        'color',
        'displacement',
        'user_id',
        'client_id',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    protected $auditInclude = [
        //'slug',
        'title',
        'body',
        'active',
        'color',
        'displacement',
        'user_id',
        'client_id',
    ];

/*    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
*/

    public function scopeActived($query)
    {
        return $query->where('active', true);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
