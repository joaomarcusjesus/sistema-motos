<?php
namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use App\Traits\User\HasAuthor;
use Cknow\Money\Money;

class Product extends Model implements AuditableInterface
{
    use HasAuthor;
    use AuditableTrait;
    use Sluggable;

    protected $fillable = [
        'slug',
        'title',
        'subtitle',
        'body',
        'price',
        'stock',
        'active',
        'category_id',
        'user_id'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    protected $auditInclude = [
        'slug',
        'title',
        'subtitle',
        'body',
        'price',
        'stock',
        'active',
        'category_id',
        'user_id'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function scopeActived($query)
    {
        return $query->where('active', true);
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function setPriceAttribute($input)
    {
        if ($input)
            $this->attributes['price'] = str_replace(['.', ',', 'R$'], ['', '.', ''], $input);
    }

    public function setPricePromotionalAttribute($input)
    {
        if ($input)
            $this->attributes['price_promotional'] = str_replace(['.', ',', 'R$'], ['', '.', ''], $input);
    }

    public function getMaskedPriceAttribute()
    {
      return Money::parseByDecimal($this->price, 'BRL')->formatSimple();
    }
}
