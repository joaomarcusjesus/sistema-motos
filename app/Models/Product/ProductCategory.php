<?php
namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use OwenIt\Auditing\Auditable as AuditableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use App\Traits\User\HasAuthor;

class ProductCategory extends Model implements AuditableInterface
{
    use HasAuthor;
    use AuditableTrait;
    use Sluggable;

    protected $table = 'products_categories';

    protected $fillable = [
        'slug',
        'title',
        'active',
        'user_id'
    ];

    protected $casts = [
        'active' => 'boolean'
    ];

    protected $auditInclude = [
        'slug',
        'title',
        'active',
        'user_id'
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function scopeActived($query)
    {
        return $query->where('active', true);
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

}
