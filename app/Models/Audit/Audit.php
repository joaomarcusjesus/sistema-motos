<?php

namespace App\Models\Audit;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Audit as AuditContract;
use OwenIt\Auditing\Audit as AuditTrait;
use Carbon\Carbon;

use App\Presenters\Audit\UrlPresenter;

class Audit extends Model implements AuditContract
{
    use AuditTrait;

    protected $guarded = [];

    protected $casts = [
        'old_values' => 'json',
        'new_values' => 'json'
    ];

    /**
     * Scopes
     */
    public function scopeWherePeriod($query, $date = null)
    {
        if ($date) {
            $date = Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');
            return $query->whereDate('whereDate', $date);
        }
    }

    public function scopeWhereUser($query, $user = null)
    {
        if ($user) {
            return $query->where('user_id', $user);
        }
    }

    public function scopeWhereType($query, $type = null)
    {
        if ($type) {
            return $query->where('auditable_type', $type);
        }
    }

    public function scopeWhereAction($query, $action = null)
    {
        if ($action) {
            return $query->where('event', $action);
        }
    }

    /**
     * Accesors
     */

    public function getCreatedAttribute() {
        return $this->created_at->formatLocalized('%d de %B de %Y às %H:%M');
    }

    public function getTypeNameAttribute()
    {
        $auditableType = strtolower(class_basename($this->auditable_type));

        return trans("audit.models.{$auditableType}") ?? 'Indefinido';
    }

    public function getActionNameAttribute()
    {
        return trans("audit.actions.{$this->event}") ?? 'Indefinido';
    }
}
