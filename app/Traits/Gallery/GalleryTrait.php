<?php

namespace App\Traits\Gallery;

trait GalleryTrait
{
    public function getGalleryAttribute()
    {
        $gallery = collect();

        $this->gallery_image->each(function ($item, $key) use($gallery) {
            $gallery->push([
                'type'        => 'photo',
                'id'          => $item->id,
                'name'        => $item->name,
                'thumb'       => $item->getUrl('thumb'),
                'photo'       => $item->getUrl('photo'),
                'created_at'  => $item->created_at
            ]);
        });

        return $gallery->sortBy('created_at');
    }
}
