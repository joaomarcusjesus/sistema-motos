<?php

namespace App\Traits\User;

use App\Models\Auth\User;

trait HasAuthor
{
    public static function bootHasAuthor()
    {
        static::creating(function ($model) {

            if(app()->environment() == 'local'){
                $model->attributes['user_id'] = 1;

                return;
            }

            $model->attributes['user_id'] = auth()->user()->id;

        });
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id')->select('id', 'first_name', 'last_name');
    }
}
