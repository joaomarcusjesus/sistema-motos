<?php

namespace App\Traits\Actived;

trait ActiveTrait
{
    public function scopeActived($query, $status = true)
    {
        return $query->where('active', $status);
    }
}
