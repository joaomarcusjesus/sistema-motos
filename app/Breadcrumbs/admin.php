<?php

// Dashboard
Breadcrumbs::for('admin.dashboard.index', function ($trail) {
  $trail->push('Dashboard', route('admin.dashboard.index'));
});

// Analytics
Breadcrumbs::for('admin.analytics.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Acessos', route('admin.analytics.index'));
});

// Audit
Breadcrumbs::for('admin.audits.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Auditoria', route('admin.audits.index'));
});

Breadcrumbs::for('admin.audits.show', function ($trail, $id) {
  $trail->parent('admin.audits.index');
  $trail->push('Log', route('admin.audits.show', ['id' => $id]));
});

// Account
Breadcrumbs::for('admin.account.show', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Perfil', route('admin.account.show'));
});

Breadcrumbs::for('admin.account.edit', function ($trail) {
  $trail->parent('admin.account.show');
  $trail->push('Editar', route('admin.account.edit'));
});

// Permissions
Breadcrumbs::for('admin.permissions.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Permissões', route('admin.permissions.index'));
});

Breadcrumbs::for('admin.permissions.create', function ($trail) {
  $trail->parent('admin.permissions.index');
  $trail->push('Adicionar', route('admin.permissions.create'));
});

Breadcrumbs::for('admin.permissions.edit', function ($trail, $id) {
  $trail->parent('admin.permissions.index');
  $trail->push('Editar', route('admin.permissions.edit', ['id' => $id]));
});

// Roles
Breadcrumbs::for('admin.roles.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Grupos', route('admin.roles.index'));
});

Breadcrumbs::for('admin.roles.create', function ($trail) {
  $trail->parent('admin.roles.index');
  $trail->push('Adicionar', route('admin.roles.create'));
});

Breadcrumbs::for('admin.roles.edit', function ($trail, $id) {
  $trail->parent('admin.roles.index');
  $trail->push('Editar', route('admin.roles.edit', ['id' => $id]));
});

// Users
Breadcrumbs::for('admin.users.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Usuários', route('admin.users.index'));
});

Breadcrumbs::for('admin.users.create', function ($trail) {
  $trail->parent('admin.users.index');
  $trail->push('Adicionar', route('admin.users.create'));
});

Breadcrumbs::for('admin.users.edit', function ($trail, $id) {
  $trail->parent('admin.users.index');
  $trail->push('Editar', route('admin.users.edit', ['id' => $id]));
});

// Messages
Breadcrumbs::for('admin.messages.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Sac', route('admin.messages.index'));
});

Breadcrumbs::for('admin.messages.show', function ($trail, $uuid) {
  $trail->parent('admin.messages.index');
  $trail->push('Mensagem', route('admin.messages.show', ['uuid' => $uuid]));
});

// Newsletter
Breadcrumbs::for('admin.newsletter.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Newsletter', route('admin.newsletter.index'));
});

// Products_Categories
Breadcrumbs::for('admin.products_categories.index', function ($trail) {
  $trail->parent('admin.products.index');
  $trail->push('Categorias', route('admin.products_categories.index'));
});

Breadcrumbs::for('admin.products_categories.create', function ($trail) {
  $trail->parent('admin.products_categories.index');
  $trail->push('Adicionar', route('admin.products_categories.create'));
});

Breadcrumbs::for('admin.products_categories.edit', function ($trail, $id) {
  $trail->parent('admin.products_categories.index');
  $trail->push('Editar', route('admin.products_categories.edit', ['id' => $id]));
});

// Products
Breadcrumbs::for('admin.products.index', function ($trail) {
    $trail->parent('admin.dashboard.index');
    $trail->push('Produtos', route('admin.products.index'));
  });

  Breadcrumbs::for('admin.products.create', function ($trail) {
    $trail->parent('admin.products.index');
    $trail->push('Adicionar', route('admin.products.create'));
  });

  Breadcrumbs::for('admin.products.edit', function ($trail, $id) {
    $trail->parent('admin.products.index');
    $trail->push('Editar', route('admin.products.edit', ['id' => $id]));
  });

// Material
Breadcrumbs::for('admin.materials.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Material', route('admin.materials.index'));
});

Breadcrumbs::for('admin.materials.create', function ($trail) {
  $trail->parent('admin.materials.index');
  $trail->push('Adicionar', route('admin.materials.create'));
});

Breadcrumbs::for('admin.materials.edit', function ($trail, $id) {
  $trail->parent('admin.materials.index');
  $trail->push('Editar', route('admin.materials.edit', ['id' => $id]));
});

// Services
Breadcrumbs::for('admin.services.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Servicos', route('admin.services.index'));
});

Breadcrumbs::for('admin.services.create', function ($trail) {
  $trail->parent('admin.services.index');
  $trail->push('Adicionar', route('admin.services.create'));
});

Breadcrumbs::for('admin.services.edit', function ($trail, $id) {
  $trail->parent('admin.services.index');
  $trail->push('Editar', route('admin.services.edit', ['id' => $id]));
});

// Motorcycles
Breadcrumbs::for('admin.motorcycles.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Motos', route('admin.motorcycles.index'));
});

Breadcrumbs::for('admin.motorcycles.create', function ($trail) {
  $trail->parent('admin.motorcycles.index');
  $trail->push('Adicionar', route('admin.motorcycles.create'));
});

Breadcrumbs::for('admin.motorcycles.edit', function ($trail, $id) {
  $trail->parent('admin.motorcycles.index');
  $trail->push('Editar', route('admin.motorcycles.edit', ['id' => $id]));
});

// Clients
Breadcrumbs::for('admin.clients.index', function ($trail) {
  $trail->parent('admin.dashboard.index');
  $trail->push('Clientes', route('admin.clients.index'));
});

Breadcrumbs::for('admin.clients.create', function ($trail) {
  $trail->parent('admin.clients.index');
  $trail->push('Adicionar', route('admin.clients.create'));
});

Breadcrumbs::for('admin.clients.edit', function ($trail, $id) {
  $trail->parent('admin.clients.index');
  $trail->push('Editar', route('admin.clients.edit', ['id' => $id]));
});
