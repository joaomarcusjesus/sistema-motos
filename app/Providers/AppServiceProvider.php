<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register app services
     */
    public function register()
    {
        $this->registerHelpers();
    }

    /**
     * Boot app services
     */
    public function boot() {}

    /**
     * Register helpers
     */
    private function registerHelpers()
    {
        require_once __DIR__ . '/../Helpers/helper.php';
    }
}
