<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Models\Setting\Setting;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Register
     * @return void
     */
    public function register()
    {
        $this->registerSettings();
    }

    /**
     * Boot
     * @return void
     */
    public function boot()
    {
        $this->checkSettingsTable();
    }

    private function checkSettingsTable()
    {

    }

    private function registerSettings()
    {

    }
}
