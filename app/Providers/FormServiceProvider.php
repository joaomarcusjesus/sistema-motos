<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Boot
     * @return void
     */
    public function boot()
    {
        Form::component('switch', 'admin.partials.form._switch', ['name', 'value' => null, 'label' => null, 'attributes' => []]);
    }
}
