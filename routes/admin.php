<?php

/**
 * Admin Routes
 */
Route::prefix('admin')->name('admin.')->group(function(){
    /**
     * Verification
     */
    Route::name('verification.verify')->get('/verificar/{id}', 'Admin\Session\VerificationController@verify');

    /**
     * Public Routes
     */
    Route::middleware('guest:dashboard')->group(function(){
        /**
         * Login
         */
        Route::name('session.login')->get('/acessar', 'Admin\Session\SessionController@showLoginForm');
        Route::name('session.login')->post('/acessar', 'Admin\Session\SessionController@login');

        /**
         * Forgot password
         */
        Route::name('password.forgot')->get('/esqueci-minha-senha', 'Admin\Session\ForgotPasswordController@showLinkRequestForm');
        Route::name('password.processForgot')->post('/esqueci-minha-senha', 'Admin\Session\ForgotPasswordController@sendResetLinkEmail');
        Route::name('password.reset')->get('/cadastrar-senha/{token}', 'Admin\Session\ResetPasswordController@showResetForm');
        Route::name('password.processReset')->post('/cadastrar-senha', 'Admin\Session\ResetPasswordController@reset');
    });

    /**
     * Protected Routes
     */
    Route::middleware(['auth:dashboard'])->group(function(){

        /**
         * Verification
         */
        Route::name('verification.notice')->get('/verificar', 'Admin\Session\VerificationController@show');
        Route::name('verification.resend')->get('/reenviar-verificacao', 'Admin\Session\VerificationController@resend');

        /**
         * First access
         */
        Route::name('activation.create')->get('/primeiro-acesso', 'Admin\Session\ActivationController@showActivationForm');
        Route::name('activation.processCreate')->post('/primeiro-acesso', 'Admin\Session\ActivationController@store');

        /**
         * Logout
         */
        Route::name('session.logout')->get('/sair', 'Admin\Session\SessionController@logout');

        /**
         * Check first access
         */
        Route::middleware(['first.access', 'verified'])->group(function(){
            /**
             * Dashboard
             */
            Route::name('dashboard.index')->get('/', 'Admin\Dashboard\DashboardController@index');

            /**
             * Account
             */
            Route::prefix('perfil')->group(function(){
                Route::name('account.show')->get('/', 'Admin\Users\AccountController@show');
                Route::name('account.edit')->get('/atualizar', 'Admin\Users\AccountController@edit');
                Route::name('account.update')->put('/atualizar', 'Admin\Users\AccountController@update');
            });

            /**
             * Permissions
             */
            Route::prefix('permissoes')->group(function(){
                Route::name('permissions.index')->get('/', 'Admin\Users\PermissionController@index');
                Route::name('permissions.create')->get('/adicionar', 'Admin\Users\PermissionController@create');
                Route::name('permissions.store')->post('/adicionar', 'Admin\Users\PermissionController@store');
                Route::name('permissions.edit')->get('/editar/{id}', 'Admin\Users\PermissionController@edit');
                Route::name('permissions.update')->put('/editar/{id}', 'Admin\Users\PermissionController@update');
                Route::name('permissions.destroy')->delete('/excluir/{id}', 'Admin\Users\PermissionController@destroy');
            });

            /**
             * Roles
             */
            Route::prefix('grupos')->group(function(){
                Route::name('roles.index')->get('/', 'Admin\Users\RoleController@index');
                Route::name('roles.create')->get('/adicionar', 'Admin\Users\RoleController@create');
                Route::name('roles.store')->post('/adicionar', 'Admin\Users\RoleController@store');
                Route::name('roles.edit')->get('/editar/{id}', 'Admin\Users\RoleController@edit');
                Route::name('roles.update')->put('/editar/{id}', 'Admin\Users\RoleController@update');
            });

            /**
             * Users
             */
            Route::prefix('usuarios')->group(function(){
                Route::name('users.index')->get('/', 'Admin\Users\UserController@index');
                Route::name('users.create')->get('/adicionar', 'Admin\Users\UserController@create');
                Route::name('users.store')->post('/adicionar', 'Admin\Users\UserController@store');
                Route::name('users.edit')->get('/editar/{id}', 'Admin\Users\UserController@edit');
                Route::name('users.update')->put('/editar/{id}', 'Admin\Users\UserController@update');
                Route::name('users.activate')->get('/ativar/{id}', 'Admin\Users\UserController@activate');
                Route::name('users.deactivate')->get('/desativar/{id}', 'Admin\Users\UserController@deactivate');
                Route::name('users.destroy')->delete('/excluir/{id}', 'Admin\Users\UserController@destroy');

            });

            /**
             * Audit
             */
            Route::prefix('auditoria')->group(function(){
                Route::name('audits.index')->get('/', 'Admin\Audits\AuditController@index');
                Route::name('audits.show')->get('/{id}', 'Admin\Audits\AuditController@show');
            });

            /*
             * Crosses
             */
            Route::prefix('materials')->group(function(){
                Route::name('materials.index')->get('/', 'Admin\Materials\MaterialController@index');
                Route::name('materials.create')->get('/adicionar', 'Admin\Materials\MaterialController@create');
                Route::name('materials.store')->post('/adicionar', 'Admin\Materials\MaterialController@store');
                Route::name('materials.edit')->get('/editar/{id}', 'Admin\Materials\MaterialController@edit');
                Route::name('materials.update')->put('/editar/{id}', 'Admin\Materials\MaterialController@update');
                Route::name('materials.destroy')->delete('/excluir/{id}', 'Admin\Materials\MaterialController@destroy');
            });

            /**
             * Services
             */
            Route::prefix('servicos')->group(function(){
                Route::name('services.index')->get('/', 'Admin\Services\ServiceController@index');
                Route::name('services.create')->get('/adicionar', 'Admin\Services\ServiceController@create');
                Route::name('services.store')->post('/adicionar', 'Admin\Services\ServiceController@store');
                Route::name('services.edit')->get('/editar/{id}', 'Admin\Services\ServiceController@edit');
                Route::name('services.update')->put('/editar/{id}', 'Admin\Services\ServiceController@update');
                Route::name('services.destroy')->delete('/excluir/{id}', 'Admin\Services\ServiceController@destroy');
            });

            /**
             * Motorcycles
             */
            Route::prefix('motos')->group(function(){
                Route::name('motorcycles.index')->get('/', 'Admin\Motorcycles\MotorcycleController@index');
                Route::name('motorcycles.create')->get('/adicionar', 'Admin\Motorcycles\MotorcycleController@create');
                Route::name('motorcycles.store')->post('/adicionar', 'Admin\Motorcycles\MotorcycleController@store');
                Route::name('motorcycles.edit')->get('/editar/{id}', 'Admin\Motorcycles\MotorcycleController@edit');
                Route::name('motorcycles.update')->put('/editar/{id}', 'Admin\Motorcycles\MotorcycleController@update');
                Route::name('motorcycles.destroy')->delete('/excluir/{id}', 'Admin\Motorcycles\MotorcycleController@destroy');
            });

            /**
             * Clients
             */
            Route::prefix('clientes')->group(function(){
                Route::name('clients.index')->get('/', 'Admin\Clients\ClientController@index');
                Route::name('clients.create')->get('/adicionar', 'Admin\Clients\ClientController@create');
                Route::name('clients.store')->post('/adicionar', 'Admin\Clients\ClientController@store');
                Route::name('clients.edit')->get('/editar/{id}', 'Admin\Clients\ClientController@edit');
                Route::name('clients.update')->put('/editar/{id}', 'Admin\Clients\ClientController@update');
                Route::name('clients.destroy')->delete('/excluir/{id}', 'Admin\Clients\ClientController@destroy');
            });

            /**
             * Products
             */
             Route::prefix('produtos')->group(function(){

                Route::name('products.index')->get('/', 'Admin\Products\ProductController@index');
                Route::name('products.create')->get('/adicionar', 'Admin\Products\ProductController@create');
                Route::name('products.store')->post('/adicionar', 'Admin\Products\ProductController@store');
                Route::name('products.edit')->get('/editar/{id}', 'Admin\Products\ProductController@edit');
                Route::name('products.update')->put('/editar/{id}', 'Admin\Products\ProductController@update');
                Route::name('products.destroy')->delete('/excluir/{id}', 'Admin\Products\ProductController@destroy');

                /**
                 * Products_Categories
                 */
                Route::prefix('/categorias')->group(function(){
                    Route::name('products_categories.index')->get('/', 'Admin\Products\ProductCategoryController@index');
                    Route::name('products_categories.create')->get('/adicionar', 'Admin\Products\ProductCategoryController@create');
                    Route::name('products_categories.store')->post('/adicionar', 'Admin\Products\ProductCategoryController@store');
                    Route::name('products_categories.edit')->get('/editar/{id}', 'Admin\Products\ProductCategoryController@edit');
                    Route::name('products_categories.update')->put('/editar/{id}', 'Admin\Products\ProductCategoryController@update');
                    Route::name('products_categories.destroy')->delete('/excluir/{id}', 'Admin\Products\ProductCategoryController@destroy');
                });
            });
        });
    });
});
