<?php

/**
 * Home
 */
Route::name('home.index')->get('/', function () {
    return redirect()->route('admin.dashboard.index');
});

/**
 * Contact
 */
Route::name('contact.index')->get('/contato', 'Front\PageController@contact');

/**
 * Sitemap
 */
Route::name('sitemap')->get('/sitemap', 'Front\SitemapController@index');
