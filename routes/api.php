<?php

/**
 * Admin group
 */
Route::middleware(['auth:dashboard'])->name('admin.')->group(function(){
  /**
   * Media routes
   */
  Route::prefix('media')->group(function(){
    Route::name('media.index')->get('/images', 'Api\MediaLibrary\MediaLibraryController@index');
    Route::name('media.store')->post('/images/adicionar', 'Api\MediaLibrary\MediaLibraryController@store');
  });
});

Route::name('api.')->group(function(){
    /**
     * Newsletter
     */
    Route::name('newsletter.store')->post('/cadastro-newsletter', 'Api\Newsletter\NewsletterController@store');

    /**
     * Messages
     */
    Route::name('message.store')->post('/enviar-mensagem', 'Api\Messages\MessageController@storeContact');

    /**
     * Places
     */
    Route::name('places.states')->get('/estados', 'Api\Places\PlaceController@getStates');
    Route::name('places.cities')->get('/cidades', 'Api\Places\PlaceController@getCities');
    Route::name('places.neighborhoods')->get('/bairros', 'Api\Places\PlaceController@getNeighborhoods');
});
