<?php

return [

    'meta' => [
        'defaults'       => [
            'title'        => env('APP_NAME', 'Laravel'),
            'titleBefore'  => false,
            'description'  => false,
            'separator'    => ' - ',
            'keywords'     => [],
            'canonical'    => false,
            'robots'       => false,
        ],

        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],

    'opengraph' => [
        'defaults' => [
            'title'       => env('APP_NAME', 'Laravel'),
            'description' => false,
            'url'         => false,
            'type'        => false,
            'site_name'   => false,
            'images'      => [],
        ],
    ],

    'twitter' => [
        'defaults' => [
            //'card'        => '',
            //'site'        => '',
        ],
    ],

    'json-ld' => [
        'defaults' => [
            'title'       => env('APP_NAME', 'Laravel'),
            'description' => false,
            'url'         => false,
            'type'        => 'WebPage',
            'images'      => [],
        ],
    ],

];
