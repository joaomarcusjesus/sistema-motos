<?php

return [

    'locale' => config('app.locale', 'pt_BR'),
    'currency' => config('app.currency', 'BRL'),

];
