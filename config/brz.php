<?php

return [

    'modules' => [

        [
            'name' => 'Clientes',
            'name_singular' => 'Cliente',
            'path' => 'clientes',
            'alias' => 'clients',
            'permission' => 'view_clients',
            'icon' => 'icon-users-before'
        ],

        [
            'name' => 'Motos',
            'name_singular' => 'Moto',
            'path' => 'motos',
            'alias' => 'motorcycles',
            'permission' => 'view_motorcycles',
            'icon' => 'icon-motorcycle-before'
        ],

        [
            'name' => 'Serviços',
            'name_singular' => 'Serviço',
            'path' => 'servicos',
            'alias' => 'services',
            'permission' => 'view_services',
            'icon' => 'icon-tools-before'
        ],

        [
            'name' => 'Produtos',
            'name_singular' => 'Produto',
            'path' => 'produtos',
            'alias' => 'products',
            'permission' => 'view_products',
            'icon' => 'icon-archive-before'
        ],

        [
            'name' => 'Usuários',
            'name_singular' => 'Usuário',
            'path' => 'usuarios',
            'alias' => 'users',
            'permission' => 'view_users',
            'icon' => 'icon-users-cog-before'
        ],

        [
            'name' => 'Grupos',
            'name_singular' => 'Grupo',
            'path' => 'grupos',
            'alias' => 'roles',
            'permission' => 'view_roles',
            'icon' => 'icon-user-tag-before'
        ],

        [
            'name' => 'Permissões',
            'name_singular' => 'Permissão',
            'path' => 'permissoes',
            'alias' => 'permissions',
            'permission' => 'view_permissions',
            'icon' => 'icon-lock-before'
        ],
    ],

    'emails' => [
        'default' => [
            'from_name' => 'Make the Next Technology',
            'from_email' => 'reply@makethenexttechnology.com.br'
        ]
    ],

    'webmaster' => 'https://brz.digital'

];
