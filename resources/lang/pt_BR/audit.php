<?php

return [

    'models' => [
        'client'      => 'Cliente',
        'motorcycle'      => 'Moto',
        'product'      => 'Produto',
        'productCategory'      => 'Categoria',
        'service'      => 'Serviço',
    ],

    'actions' => [
        'created'       => 'Criação',
        'updated'       => 'Alteração',
        'deleted'       => 'Exclusão',
        'restored'      => 'Reativação'
    ]

];
