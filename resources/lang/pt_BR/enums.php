<?php

use App\Enums\Taxonomy\TaxonomyType;
use App\Enums\Message\Department;

return [

    TaxonomyType::class => [
        TaxonomyType::CATEGORY      => 'Categoria',
    ],

    Department::class => [
        Department::CONTACT         => 'Geral',
        Department::RH              => 'RH',
        Department::FINANCE         => 'Financeiro',
        Department::COMPLAINT       => 'Reclamação',
        Department::SUGGESTION      => 'Sugestão',
    ],

];
