{{ Form::open(['method' => 'post', 'class' => 'form -default row', 'route' => 'api.message.store', 'data-module' => 'Form']) }}
    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('name', 'Seu nome', ['class' => 'form-label']) }}
            {{ Form::text('name', old('name'), ['class' => 'form-control input js-validate']) }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('email', 'Seu e-mail', ['class' => 'form-label']) }}
            {{ Form::email('email', old('email'), ['class' => 'form-control input js-validate']) }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('phone', 'Telefone', ['class' => 'form-label']) }}
            {{ Form::tel('phone', old('phone'), ['class' => 'form-control input js-mask-phone js-validate']) }}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {{ Form::label('subject', 'Assunto', ['class' => 'form-label']) }}
            {{ Form::text('subject', old('subject'), ['class' => 'form-control input js-validate']) }}
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            {{ Form::label('body', 'Mensagem', ['class' => 'form-label']) }}
            {{ Form::textarea('body', old('body'), ['class' => 'form-control textarea js-validate', 'rows' => 4]) }}
        </div>
    </div>

    <div class="col-md-12">
        {{ Form::button('Enviar mensagem', ['class' => 'submit js-send-form', 'type' => 'submit']) }}
    </div>
{{ Form::close() }}
