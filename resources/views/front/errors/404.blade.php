@extends('theme.layouts.error')

@section('content')
@include('front.errors.partials._content', [
  'errorStatus' => 404,
  'errorMessage' => 'Não conseguimos encontrar a página que você procura.'
])
@stop
