@extends('theme.layouts.error')

@section('content')
@include('front.errors.partials._content', [
  'errorStatus' => 500,
  'errorMessage' => 'Ocorreu um erro no servidor.'
])
@stop
