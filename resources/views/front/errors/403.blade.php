@extends('theme.layouts.error')

@section('content')
@include('front.errors.partials._content', [
  'errorStatus' => 403,
  'errorMessage' => 'Você não tem permissão para acessar esta página.'
])
@stop
