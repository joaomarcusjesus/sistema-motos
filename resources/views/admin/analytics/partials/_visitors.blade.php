<!-- pages -->
<section class="card card-default -shadow widget widget-chart">
    <header class="card-header bg-info">
        <h2 class="title icon-chart-line-before">Visitas e Pageviews</h2>
    </header>
    <article class="card-body">
        <div style="width: 100%; clear: both;" class="mb-3">
            <div class="chart-media">
                <ve-line height="300px" :data="chartData" :colors="['#78c62b', '#00a9d8']" :settings="chartSettings"></ve-line>
            </div>
        </div>
    </article>
</section>

@push('scripts')
<script>
    var chartSettings = {
        labelMap: {
            vt: 'Clients',
            pv: 'Produtos'
        },
        area: true
    };

    var chartData = {
        columns: ['date', 'vt', 'pv'],
        rows: {!! json_encode($visitorsData) !!}
    };
</script>
@endpush
