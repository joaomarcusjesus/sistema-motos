<!-- browsers -->
<section class="card card-default -shadow widget widget-status">
    <header class="card-header bg-warning">
        <h2 class="title icon-browser-before">Browsers</h2>
    </header>
    <article class="card-body -with-scroll">
        <div class="scrollbar">
            @if($browserData->count() > 0)
            <table class="table -dashed">
                <tbody class="table-tbody">
                    @foreach($browserData as $browserRow)
                    <tr class="table-row">
                        <td class="table-col" data-th="Navegador">
                            <span class="text">{{ $browserRow['browser'] }}</span>
                        </td>
                        <td class="table-col" data-th="Sessões">
                            <span class="text">{{ $browserRow['sessions'] }}</span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p class="text">Nenhum registro encontrado</p>
            @endif
        </div>
    </article>
</section>
