<!-- pages -->
<section class="card card-default -shadow widget widget-traffic">
    <header class="card-header bg-warning">
        <h2 class="title icon-map-signs-before">Tráfego</h2>
    </header>
    <article class="card-body -with-scroll">
        <div class="scrollbar">
            @if($referrerData->count() > 0)
            <table class="table -dashed">
                <tbody class="table-tbody">
                    @foreach($referrerData as $referrerRow)
                    <tr class="table-row">
                        <td class="table-col" data-th="Página">
                            <span class="text">{{ $referrerRow['url'] }}</span>
                        </td>
                        <td class="table-col" data-th="PageViews">
                            <span class="text">{{ $referrerRow['pageViews'] }}</span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p class="text">Nenhum registro encontrado</p>
            @endif
        </div>
    </article>
</section>
