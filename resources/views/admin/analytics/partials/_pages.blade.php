<!-- pages -->
<section class="card card-default -shadow widget widget-status">
    <header class="card-header bg-info">
        <h2 class="title icon-eye-before">Páginas mais acessadas</h2>
    </header>
    <article class="card-body -with-scroll">
        <div class="scrollbar">
            @if($pagesData->count() > 0)
            <table class="table -dashed">
                <tbody class="table-tbody">
                    @foreach($pagesData as $pageRow)
                    <tr class="table-row">
                        <td class="table-col" data-th="Página">
                            <span class="text">{{ $pageRow['url'] }}</span>
                        </td>
                        <td class="table-col" data-th="PageViews">
                            <span class="text">{{ $pageRow['pageViews'] }}</span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p class="text">Nenhum registro encontrado</p>
            @endif
        </div>
    </article>
</section>
