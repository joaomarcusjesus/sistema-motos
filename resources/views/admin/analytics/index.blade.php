@extends('admin.layouts.app')

@section('content')
<section class="module">
    <header class="module-heading">
        <div class="module-heading-inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-5">
                        <div class="title-area">
                            <h2 class="title align-middle">Acessos</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <article class="module-content">
        <div data-module="Slide" class="dashboard container-fluid js-swiper" data-only-mobile="true" data-per-view-sm="2" data-effect="slide">
            <div class="wrapper">
                <div class="slide-item col-sm-6 col-lg-4 col-xl-9 mb-4">
                    @include('admin.analytics.partials._visitors')
                </div>

                <div class="slide-item col-sm-6 col-lg-8 col-xl-3 mb-4">
                    @include('admin.analytics.partials._origins')
                </div>

                <div class="slide-item col-sm-6 col-lg-4 col-xl-9 mb-4">
                    @include('admin.analytics.partials._pages')
                </div>

                <div class="slide-item col-sm-6 col-lg-8 col-xl-3 mb-4">
                    @include('admin.analytics.partials._browsers')
                </div>
            </div>
        </div>
    </article>
</section>
@stop
