@extends('admin.layouts.app')

@section('content')
<section class="module">
    <header class="module-heading">
        <div class="module-heading-inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-5">
                        <div class="title-area">
                            <h2 class="title align-middle">Auditoria</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <aside class="util mb-4">
        <div class="container-fluid">
            <div class="card card-default -shadow">
                <div class="card-body -search">
                    @include('admin.audits.partials._search')
                </div>
            </div>
        </div>
    </aside>

    <article class="module-content">
        <div class="container-fluid">
            <div class="card card-default -shadow">
                <div class="card-body p-0">
                    <table class="table table-striped table-bordered -list">
                        <thead class="table-thead">
                            <tr class="table-row">
                                <th class="table-col">
                                    <span class="text">Usuário</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Tipo</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Ação</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Horário</span>
                                </th>
                                <th class="table-col -actions">
                                    <span class="text">Ações</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="table-tbody">
                            @foreach($results as $result)
                            <tr class="table-row">
                                <td class="table-col" data-th="Usuário">
                                    <span class="text">{{ $result->user->full_name }}</span>
                                </td>
                                <td class="table-col" data-th="Tipo">
                                    <span class="badge badge-warning -status">{{ $result->type_name }}</span>
                                </td>
                                <td class="table-col" data-th="Ação">
                                    <span class="badge badge-info -status">{{ $result->action_name }}</span>
                                </td>
                                <td class="table-col" data-th="Horário">
                                    <span class="text">{{ $result->created_at->format('d/m/Y H:i') }}</span>
                                </td>
                                <td class="table-col -actions" data-th="Ações">
                                    <a href="{{ route('admin.audits.show', ['id' => $result->id]) }}" class="btn btn-link -success icon-eye-before js-tooltip" data-placement="top" title="Visualizar">
                                        <span class="sr-only">Visualizar</span>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>

    <footer class="module-footing">
        {{ $results->render('admin.partials._pagination') }}
    </footer>
</section>
@stop
