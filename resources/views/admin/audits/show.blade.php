@extends('admin.layouts.app')

@section('content')
<section class="module">
    <header class="module-heading">
        <div class="module-heading-inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-5">
                        <div class="title-area">
                            <h2 class="title align-middle">Log</h2>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="actions text-right">
                            <div class="actions-content">
                                <a href="{{ route('admin.audits.index') }}" class="btn btn-danger icon-chevron-left-before" title="Voltar">
                                    <span>Voltar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <article class="module-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 mb-4">
                    @include('admin.audits.partials._user')
                    @include('admin.audits.partials._details')
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    @include('admin.audits.partials._log')
                </div>
            </div>
        </div>
    </article>
</section>
@stop
