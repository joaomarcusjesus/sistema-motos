<div class="search">
    {{ Form::open(['route' => 'admin.audits.index', 'class' => 'form -default', 'method' => 'GET']) }}
    <div class="row">
        <div class="form-group col-sm-6 col-md-4 col-lg-2">
            {{ Form::label('data', 'Data', ['class' => 'sr-only']) }}
            {{ Form::text('data', request('data'), ['class' => 'form-control js-datepicker', 'placeholder' => 'Data', 'data-date-format' => 'd-m-Y']) }}
        </div>

        <div class="form-group col-sm-6 col-md-8 col-lg-3">
            {{ Form::label('usuario', 'Usuário', ['class' => 'sr-only']) }}
            {{ Form::select('usuario', $users, request('usuario'), ['class' => 'form-control', 'placeholder' => 'Usuário']) }}
        </div>

        <div class="form-group col-sm-6 col-md-4 col-lg-3">
            {{ Form::label('tipo', 'Tipo', ['class' => 'sr-only']) }}
            {{ Form::select('tipo', trans('audit.models'), request('tipo'), ['class' => 'form-control', 'placeholder' => 'Tipo']) }}
        </div>

        <div class="form-group col-sm-6 col-md-4 col-lg-2">
            {{ Form::label('acao', 'Ação', ['class' => 'sr-only']) }}
            {{ Form::select('acao', trans('audit.actions'), request('acao'), ['class' => 'form-control', 'placeholder' => 'Ação']) }}
        </div>

        <div class="form-group col-6 col-md-2 col-lg-1">
            <button type="reset" class="btn btn-danger btn-block icon-times-before">
                <span class="sr-only">Resetar</span>
            </button>
        </div>

        <div class="form-group col-6 col-md-2 col-lg-1">
            <button type="submit" class="btn btn-success btn-block icon-search-before">
                <span class="sr-only">Pesquisar</span>
            </button>
        </div>
    </div>
    {{ Form::close() }}
</div>
