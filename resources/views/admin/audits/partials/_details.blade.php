<div class="card card-default -shadow mb-4">
    <div class="card-header bg-default">
        <h3 class="title icon-cog-before">Log</h3>
    </div>
    <div class="card-body">
        <p class="text text-tiny">{{ $result->created }}</p>
        <p class="text text-tiny">{{ $result->ip_address }}</p>
        <p class="text text-tiny mb-0">{{ $result->user_agent }}</p>
        <hr>
        <p class="badge badge-warning -status">{{ $result->type_name }}</p>
        <p class="badge badge-warning -status">{{ $result->action_name }}</p>
    </div>
</div>
