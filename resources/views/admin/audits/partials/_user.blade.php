<div class="card card-default -shadow mb-4">
    <div class="card-header bg-warning">
        <h3 class="title icon-user-before">Responsável</h3>
    </div>
    <div class="card-body">
        <div class="preview">
            <div class="preview-figure">
                <div class="preview-image -avatar bg-cover" style="background-image: url('{{ $result->user->photo }}');"></div>
            </div>
            <div class="preview-caption d-flex">
                <div class="align-self-center">
                    <p class="text d-block mb-2">{{ $result->user->full_name }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
