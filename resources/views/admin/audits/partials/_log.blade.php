<div class="card card-default -shadow mb-4">
    <div class="card-header bg-info">
        <h3 class="title icon-tasks-before">Detalhes</h3>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped table-bordered -list">
            <thead class="table-thead">
                <tr class="table-row">
                    <th class="table-col">
                        <span class="text">Campo</span>
                    </th>
                    @if($result->event !== 'created')
                    <th class="table-col">
                        <span class="text">Antes</span>
                    </th>
                    <th class="table-col">
                        <span class="text">Depois</span>
                    </th>
                    @else
                    <th class="table-col">
                        <span class="text">Valores</span>
                    </th>
                    @endif
                </tr>
            </thead>
            <tbody class="table-tbody">
                @foreach($result->getModified() as $key => $field)
                <tr class="table-row">
                    <td class="table-col" data-th="Atributo">
                        <span class="badge badge-info -status">{{ $key }}</span>
                    </td>
                    @if($result->event !== 'created')
                    <td class="table-col" data-th="Antes">
                        @if(isset($field['old']))
                        @if($field['old'] !== null)
                        <span class="text">{{ $field['old'] }}</span>
                        @endif
                        @endif
                    </td>
                    <td class="table-col" data-th="Depois">
                        @if(isset($field['new']))
                        @if($field['new'] !== null)
                        <span class="text">{{ $field['new'] }}</span>
                        @endif
                        @endif
                    </td>
                    @else
                    <td class="table-col" data-th="Valores">
                        @if(isset($field['new']))
                        @if($field['new'] !== null)
                        <span class="text">{{ $field['new'] }}</span>
                        @endif
                        @endif
                    </td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
