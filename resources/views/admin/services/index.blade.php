@extends('admin.layouts.app')

@section('content')
    <section class="module">
        <header class="module-heading">
            <div class="module-heading-inner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-5">
                            <div class="title-area">
                                <h2 class="title align-middle">Serviços</h2>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="actions text-right">
                                <div class="actions-content">
                                    @can('add_services')
                                        <a href="{{ route('admin.services.create') }}"
                                           class="btn btn-success icon-plus-before" title="Adicionar">
                                            <span>Adicionar</span>
                                        </a>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <aside class="util mb-4">
            <div class="container-fluid">
                <div class="card card-default -shadow">
                    <div class="card-body -search">
                        @include('admin.services.partials._search')
                    </div>
                </div>
            </div>
        </aside>

        <article class="module-content">
            <div class="container-fluid">
                <div class="card card-default -shadow">
                    <div class="card-body p-0">
                        <table class="table table-striped table-bordered -list">
                            <thead class="table-thead">
                            <tr class="table-row">
                                <th class="table-col">
                                    <span class="text">Nome do Serviço</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Descrição</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Cliente</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Data Criação</span>
                                </th>
                                <th class="table-col -actions">
                                    <span class="text">Ações</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="table-tbody">
                            @foreach($results as $result)
                                <tr class="table-row" id="result-{{ $result->id }}">
                                    <td class="table-col" data-th="Nome do Serviço">
                                        <span class="text">{{ $result->title }}</span>
                                    </td>
                                    <td class="table-col" data-th="Descrição">
                                        @if(isset($result->body))
                                            <span class="text">{{ ($result->body) }}</span>
                                        @else
                                            <span class="text">Serviço sem descrição</span>
                                        @endif
                                    </td>
                                    <td class="table-col" data-th="Cliente">
                                        <span class="text">{{ $result->client->full_name }}</span>
                                    </td>
                                    <td class="table-col" data-th="Data Criação">
                                        <span class="text">{{ $result->created_at->format('d/m/yy H:i') }}</span>
                                    </td>
                                    <td class="table-col -actions" data-th="Ações">
                                        @can('edit_services')
                                            <a href="{{ route('admin.services.edit', ['id' => $result->id]) }}"
                                               class="btn btn-link -success icon-pencil-before js-tooltip"
                                               data-placement="top" title="Editar">
                                                <span class="sr-only">Editar</span>
                                            </a>
                                        @endcan

                                        @can('delete_services')
                                            <a href="{{ route('admin.services.destroy', ['id' => $result->id]) }}"
                                               data-target="#result-{{ $result->id }}" data-type="delete"
                                               data-message="Deseja excluir o serviço {{ $result->title }}?"
                                               data-confirm="Excluir"
                                               class="js-confirm btn btn-link -danger icon-trash-alt-before js-tooltip"
                                               data-placement="top" title="Excluir">
                                                <span class="sr-only">Excluir</span>
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>

        <footer class="module-footing">
            {{ $results->render('admin.partials._pagination') }}
        </footer>
    </section>
@stop
