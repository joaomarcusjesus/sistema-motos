@extends('admin.layouts.app')

@section('content')
<section class="module">
    <header class="module-heading">
        <div class="module-heading-inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-5">
                        <div class="title-area">
                            <h2 class="title align-middle">Meu Perfil</h2>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="actions text-right">
                            <div class="actions-content">
                                <a href="{{ route('admin.account.edit') }}" class="btn btn-success icon-edit-before" title="Editar">
                                    <span>Editar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <article class="module-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 mb-4">
                    <div class="card card-default -shadow">
                        <div class="card-body">
                            <div class="preview mb-4">
                                <div class="preview-figure">
                                    <div class="preview-image -avatar bg-cover" style="background-image: url('{{ $result->photo }}');"></div>
                                </div>
                                <div class="preview-caption d-flex">
                                    <div class="align-self-center">
                                        <p class="text d-block mb-2">{{ $result->full_name }}</p>
                                        <p class="badge badge-success text-tiny mb-0 d-block">{{ $result->last_login }}</p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <p class="text -tiny mb-0">{{ $result->email }}</p>
                            <p class="text -tiny mb-3">{{ maskPhone($result->phone) }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="card card-default -shadow mb-4">
                        <div class="card-header bg-info">
                            <h3 class="title icon-clock-before">Últimas atividades</h3>
                        </div>
                        <div class="card-body">
                            {{--
                            <ul class="history">
                                <li class="history-step">
                                  <div class="order-history-date">
                                    <p class="history-date icon-calendar-before mb-0">15 de novembro de 2017</p>
                                  </div>
                                  <div class="order-history-entry">
                                    <p class="history-hour mb-0">02:16</p>
                                    <p class="history-text mb-0">removeu o usuário "jonathan".</p>
                                  </div>
                                  <div class="order-history-entry">
                                    <p class="history-hour mb-0">02:32</p>
                                    <p class="history-text mb-0">atualizou o produto "banana".</p>
                                  </div>
                                </li>
                            </ul>
                            --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>
@stop
