<div class="container-fluid">
    <div class="row">
        <div class="col-sm-5 col-md-4 col-lg-3">
            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-warning">
                    <h2 class="title icon-key-before">Acesso</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            {{ Form::label('password', 'Senha', ['class' => 'form-label']) }}
                            {{ Form::password('password', ['class' => 'form-control']) }}

                            @if($errors->has('password'))
                            <span class="help-block -error">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-12">
                            {{ Form::label('password_confirmation', 'Confirmar senha', ['class' => 'form-label']) }}
                            {{ Form::password('password_confirmation', ['class' => 'form-control']) }}

                            @if($errors->has('password_confirmation'))
                            <span class="help-block -error">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-default">
                    <h2 class="title icon-user-before">Dados pessoais</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-6 col-md-4">
                            {{ Form::label('first_name', 'Nome', ['class' => 'form-label']) }}
                            {{ Form::text('first_name', old('first_name'), ['class' => 'form-control']) }}

                            @if($errors->has('first_name'))
                            <span class="help-block -error">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-6 col-md-4">
                            {{ Form::label('last_name', 'Sobrenome', ['class' => 'form-label']) }}
                            {{ Form::text('last_name', old('last_name'), ['class' => 'form-control']) }}

                            @if($errors->has('last_name'))
                            <span class="help-block -error">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-12 col-md-4 col-lg-4">
                            {{ Form::label('photo', 'Foto', ['class' => 'form-label']) }}
                            {{ Form::file('photo', ['class' => 'js-file-input']) }}

                            @if($errors->has('photo'))
                            <span class="help-block -error">{{ $errors->first('photo') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-default">
                    <h2 class="title icon-envelope-before">Contatos</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-6 col-md-4">
                            {{ Form::label('email', 'E-mail', ['class' => 'form-label']) }}
                            {{ Form::email('email', old('email'), ['class' => 'form-control']) }}

                            @if($errors->has('email'))
                            <span class="help-block -error">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-6 col-md-4">
                            {{ Form::label('phone', 'Telefone', ['class' => 'form-label']) }}
                            <div>
                                <the-mask name="phone" value="{{ isset($result) ? $result->phone : old('phone') }}" class="form-control" type="tel" :mask="['(##) ####-####', '(##) #####-####']" />
                            </div>

                            @if($errors->has('phone'))
                            <span class="help-block -error">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
