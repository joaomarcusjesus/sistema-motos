@extends('admin.layouts.app')

@section('content')
<section class="module">
  <header class="module-heading">
    <div class="module-heading-inner">
      <div class="container-fluid">
        <div class="row">
          <div class="col-5">
            <div class="title-area">
              <h2 class="title align-middle">Grupos</h2>
            </div>
          </div>
          <div class="col-7">
            <div class="actions text-right">
              <div class="actions-content">
                <a href="{{ route('admin.roles.create') }}" class="btn btn-success icon-plus-before" title="Adicionar">
                  <span>Adicionar</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <article class="module-content">
    <div class="container-fluid">
      <div class="card card-default -shadow">
        <div class="card-body p-0">
          <table class="table table-striped table-bordered -list">
            <thead class="table-thead">
              <tr class="table-row">
                <th class="table-col">
                  <span class="text">Grupo</span>
                </th>
                <th class="table-col">
                  <span class="text">Chave</span>
                </th>
                <th class="table-col -actions">
                  <span class="text">Ações</span>
                </th>
              </tr>
            </thead>
            <tbody class="table-tbody">
              @foreach($results as $result)
              <tr class="table-row" id="result-{{ $result->id }}">
                <td class="table-col" data-th="Grupo">
                  <span class="text">{{ $result->details }}</span>
                </td>
                <td class="table-col" data-th="Chave">
                  <span class="text">{{ $result->name }}</span>
                </td>
                <td class="table-col -actions" data-th="Ações">
                  @can('edit_roles')
                  <a href="{{ route('admin.roles.edit', ['id' => $result->id]) }}" class="btn btn-link -success icon-pencil-before js-tooltip" data-placement="top" title="Editar">
                    <span class="sr-only">Editar</span>
                  </a>
                  @endcan
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </article>

  <footer class="module-footing">
  {{ $results->render('admin.partials._pagination') }}
  </footer>
</section>
@stop
