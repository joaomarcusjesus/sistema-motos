@extends('admin.layouts.app')

@section('content')
<section class="module">
  {!! Form::model($result, ['method' => 'PUT', 'route' => ['admin.roles.update',  'id' => $result->id], 'class' => 'form -default']) !!}
    <header class="module-heading">
      <div class="module-heading-inner">
        <div class="container-fluid">
          <div class="row">
            <div class="col-5">
              <div class="title-area">
                <h2 class="title align-middle">Editar Grupo</h2>
              </div>
            </div>
            <div class="col-7">
              <div class="actions text-right">
                <div class="actions-content">
                  <a href="{{ route('admin.roles.index') }}" class="btn btn-danger icon-chevron-left-before" title="Voltar">
                    <span>Voltar</span>
                  </a>
                  <button type="submit" class="btn btn-success icon-save-before" title="Atualizar">
                    <span>Atualizar</span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

    <article class="module-content">
      @include('admin.roles._form')
    </article>
  {!! Form::close() !!}
</section>
@stop
