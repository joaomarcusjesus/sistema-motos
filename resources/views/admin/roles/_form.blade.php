<div class="container-fluid">
  <div class="row">
    <div class="col-sm-5 col-md-4 col-lg-3">
      <div class="card card-default -shadow mb-4">
        <header class="card-header bg-warning">
          <h2 class="title icon-users-before">Detalhes</h2>
        </header>
        <div class="card-body">
          <div class="row">
            <div class="form-group col-sm-12">
              {!! Form::label('details', 'Grupo', ['class' => 'form-label']) !!}
              {!! Form::text('details', old('details'), ['class' => 'form-control']) !!}

              @if($errors->has('details'))
              <span class="help-block -error">{{ $errors->first('details') }}</span>
              @endif
            </div>
            <div class="form-group col-sm-12">
              {!! Form::label('name', 'Chave', ['class' => 'form-label']) !!}
              {!! Form::text('name', old('name'), ['class' => 'form-control', 'disabled' => isset($result) ? true : false]) !!}

              @if($errors->has('name'))
              <span class="help-block -error">{{ $errors->first('name') }}</span>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-7 col-md-8 col-lg-9">
      <div class="card card-default -shadow mb-4">
        <header class="card-header bg-info">
          <h2 class="title icon-users-before">Permissões</h2>
        </header>
        <div class="card-body">
          <div class="row">
            <div class="form-group col-sm-12">
              {!! Form::label('permissions[]', 'Ações', ['class' => 'form-label']) !!}

              <div class="row">
                @foreach($permissions as $permission)
                  @can($permission->name)
                  <div class="col-sm-12 col-md-6 col-lg-4 form-group">
                    <div class="custom-control custom-checkbox custom-control-inline">
                      {!! Form::checkbox('permissions[]', $permission->name, isset($result) ? $result->hasPermissionTo($permission->name) : false, ['id' => $permission->name, 'class' => 'custom-control-input']) !!}
                      {!! Form::label($permission->name, $permission->details, ['class' => 'custom-control-label']) !!}
                    </div>
                  </div>
                  @endcan
                @endforeach
              </div>

              @if($errors->has('permissions'))
              <span class="help-block -error">{{ $errors->first('permissions') }}</span>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
