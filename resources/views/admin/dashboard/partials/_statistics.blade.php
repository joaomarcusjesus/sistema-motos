<!-- statistics -->
<section class="card card-default -shadow widget widget-status">
    <header class="card-header bg-info">
        <h2 class="title icon-heartbeat-before">Estatísticas</h2>
    </header>
    <article class="card-body -with-scroll">
        <div class="scrollbar">
            <div class="row">
                <div class="tile -status col-12 col-sm-12 col-md-12 col-lg-12 col-xl-4">
                    <div class="tile-content d-table">
                        <div class="d-table-cell w-100 h-100 align-middle">
                            <span class="tile-count">128</span>
                            <span class="tile-legend">Leads</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>
