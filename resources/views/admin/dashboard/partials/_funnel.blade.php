<!-- funnel -->
<section class="card card-default -shadow widget widget-funnel">
    <header class="card-header text-center">
        <h2 class="title">Funil de vendas</h2>
    </header>
    <article class="card-body">
        <ul class="funnel -cone">
            <li class="item -color29">
                <a href="./customers.html" class="funnel-link" title="Visitantes: 0">
                    <span class="funnel-legend">Visitantes</span>
                    <span class="funnel-count">1209</span>
                </a>
            </li>
            <li class="item -color24">
                <a href="./customers.html" class="funnel-link" title="Leads: 0">
                    <span class="funnel-legend">Leads</span>
                    <span class="funnel-count">19</span>
                </a>
            </li>
            <li class="item -color22">
                <a href="./orders.html" class="funnel-link" title="Oportunidades: 0">
                    <span class="funnel-legend">Oportunidades</span>
                    <span class="funnel-count">64</span>
                </a>
            </li>
            <li class="item -color20">
                <a href="./orders.html" class="funnel-link" title="Conquistados: 0">
                    <span class="funnel-legend">Conquistados</span>
                    <span class="funnel-count">42</span>
                </a>
            </li>
            <li class="item -color50">
                <a href="./orders.html" class="funnel-link" title="Perdidos: 0">
                    <span class="funnel-legend">Perdidos</span>
                    <span class="funnel-count">3</span>
                </a>
            </li>
        </ul>
    </article>
</section>
