<!-- messages -->
<section class="card card-default -shadow widget widget-messages">
    <header class="card-header bg-warning">
        <h2 class="title icon-star-before">Últimos clientes</h2>
    </header>
    <article class="card-body -with-scroll">
        <div class="scrollbar">
{{--            <table class="table -dashed">--}}
{{--                <tbody class="table-tbody">--}}
{{--                    @foreach($messages as $message)--}}
{{--                    <tr class="table-row">--}}
{{--                        <td class="table-col">--}}
{{--                            <a href="{{ $message->url->admin_show }}" class="link d-block" title="{{ $message->subject }} - {{ $message->name }}">--}}
{{--                                <p class="text m-0">{{ $message->subject }}</p>--}}
{{--                                <span class="badge badge-warning">{{ $message->name }}</span>--}}
{{--                            </a>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    @endforeach--}}
{{--                </tbody>--}}
{{--            </table>--}}
        </div>
    </article>
    <footer class="card-footer text-center">
{{--        <a href="{{ route('admin.messages.index') }}" class="link font-weight-bold" title="Ver todas">Ver todas</a>--}}
    </footer>
</section>
