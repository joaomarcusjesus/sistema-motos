<!-- leads -->
<section class="card card-default -shadow widget widget-leads">
    <header class="card-header bg-success">
        <h2 class="title icon-user-check-before">Novos leads</h2>
    </header>
    <article class="card-body -with-scroll">
        <div class="scrollbar">
            <table class="table -dashed">
                <thead class="table-thead">
                    <tr class="table-row">
                        <th class="table-col">
                            <span class="text">Nome</span>
                        </th>
                        <th class="table-col">
                            <span class="text">Contatos</span>
                        </th>
                        <th class="table-col">
                            <span class="text">Ações</span>
                        </th>
                    </tr>
                </thead>
                <tbody class="table-tbody">
                    <tr class="table-row">
                        <td class="table-col" data-th="Nome">
                            <div class="preview -people">
                                <div class="preview-figure">
                                    <div class="preview-image bg-cover" style="background-image: url('{{ asset('/brzadmin/images/avatar.svg') }}');"></div>
                                </div>
                                <div class="preview-caption d-flex">
                                    <span class="text align-self-center">Hugo Fabricio</span>
                                </div>
                            </div>
                        </td>
                        <td class="table-col" data-th="Contatos">
                            <span class="badge badge-success -status">(83) 98805-0131</span>
                            <span class="badge badge-success -status">me@hugofabricio.com</span>
                        </td>
                        <td class="table-col" data-th="Ações">
                            <a href="#" class="btn btn-link -success icon-eye-before js-tooltip" data-placement="top" title="Visualizar">
                                <span class="sr-only">Visualizar</span>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </article>
    <footer class="card-footer text-center">
        <a href="#" class="link font-weight-bold" title="Ver todos">Ver todos</a>
    </footer>
</section>
