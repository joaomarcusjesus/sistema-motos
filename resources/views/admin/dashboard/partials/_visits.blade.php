<!-- visits -->
<section class="card card-default -shadow widget widget-count-visits">
    <header class="card-header text-center">
        <h2 class="title">Visitas ao seu site</h2>
    </header>
    <article class="card-body d-flex justify-content-center align-items-center">
        @if($visitors)
        <a href="{{ route('admin.analytics.index') }}" class="link text-success" title="{{ $visitors['pageViews'] }} visita(s) em {{ $visitors['date']->formatLocalized('%d de %B') }}"><span>{{ $visitors['pageViews'] }}</span> visualizações em<br> {{ $visitors['date']->formatLocalized('%d de %B') }}</a>
        @endif
    </article>
</section>
