@extends('admin.layouts.app')

@section('content')
<div data-module="Slide" class="dashboard container-fluid js-swiper" data-only-mobile="true" data-per-view-sm="2" data-effect="slide">
    <div class="wrapper">
        <div class="slide-item col-sm-6 col-lg-4 col-xl-9 mb-4">
            @include('admin.analytics.partials._visitors')
        </div>

        <div class="slide-item col-sm-6 col-lg-3 col-xl-3 mb-4">
            @include('admin.dashboard.partials._messages')
        </div>
    </div>
</div>
@stop
