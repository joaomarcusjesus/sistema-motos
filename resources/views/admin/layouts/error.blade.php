<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ops!</title>

    @include('admin.partials._favicons')
    @include('admin.partials._styles')
</head>
<body class="body -public" data-module="Common">
    <div id="app" class="app">
        <main class="main h-100" role="main">
            @yield('content')
        </main>

        @include('admin.partials._footer')
    </div>

    @include('admin.partials._scripts')
</body>
</html>
