@extends('admin.layouts.app')

@section('content')
    <section class="module">
        <header class="module-heading">
            <div class="module-heading-inner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-5">
                            <div class="title-area">
                                <h2 class="title align-middle">Produtos</h2>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="actions text-right">
                                <div class="actions-content">
                                    @can('add_products_submodules')
                                        <a href="{{ route('admin.products.create') }}"
                                           class="btn btn-success icon-plus-before" title="Adicionar">
                                            <span>Adicionar</span>
                                        </a>
                                    @endcan
{{--                                    @can('add_products_submodules')
                                        <div class="dropdown d-inline-block">
                                            <button class="btn btn-primary dropdown-toggle" type="button"
                                                    id="submodules" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                <span>Configurações</span>
                                            </button>

                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="submodules">
                                                <a href="{{ route('admin.products_categories.index') }}"
                                                   class="dropdown-item"
                                                   title="Categorias">Categorias</a>
                                            </div>
                                        </div>
                                    @endcan
--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <aside class="util mb-4">
            <div class="container-fluid">
                <div class="card card-default -shadow">
                    <div class="card-body -search">
                        @include('admin.products.partials._search')
                    </div>
                </div>
            </div>
        </aside>

        <article class="module-content">
            <div class="container-fluid">
                <div class="card card-default -shadow">
                    <div class="card-body p-0">
                        <table class="table table-striped table-bordered -list">
                            <thead class="table-thead">
                            <tr class="table-row">
                                <th class="table-col">
                                    <span class="text">Produtos</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Preço</span>
                                </th>
{{--                                <th class="table-col">
                                    <span class="text">Categoria</span>
                                </th>
--}}
                                <th class="table-col">
                                    <span class="text">Status</span>
                                </th>
                                <th class="table-col -actions">
                                    <span class="text">Ações</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="table-tbody">
                            @foreach($results as $result)
                                <tr class="table-row" id="result-{{ $result->id }}">
                                    <td class="table-col" data-th="Produtos">
                                        <span class="text">{{ $result->title }}</span>
                                    </td>
                                    <td class="table-col" data-th="Preço">
                                        <span class="text">{{ $result->maskedPrice }}</span>
                                    </td>
{{--                                    <td class="table-col" data-th="Categoria">
                                    @if(isset($result->category))
                                        <span class="text">{{ ($result->category->title) }}</span>
                                    @else
                                        <span class="text">Produto sem Categoria</span>
                                    @endif
                                    </td>
--}}
                                    <td class="table-col" data-th="Status">
                                        {!! isActive($result->active) !!}
                                    </td>
                                    <td class="table-col -actions" data-th="Ações">
                                        @can('edit_products')
                                            <a href="{{ route('admin.products.edit', ['id' => $result->id]) }}"
                                               class="btn btn-link -success icon-pencil-before js-tooltip"
                                               data-placement="top" title="Editar">
                                                <span class="sr-only">Editar</span>
                                            </a>
                                        @endcan

                                        @can('delete_products')
                                            <a href="{{ route('admin.products.destroy', ['id' => $result->id]) }}"
                                               data-target="#result-{{ $result->id }}" data-type="delete"
                                               data-message="Deseja excluir o produto {{ $result->title }}?"
                                               data-confirm="Excluir"
                                               class="js-confirm btn btn-link -danger icon-trash-alt-before js-tooltip"
                                               data-placement="top" title="Excluir">
                                                <span class="sr-only">Excluir</span>
                                            </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>

        <footer class="module-footing">
            {{ $results->render('admin.partials._pagination') }}
        </footer>
    </section>
@stop
