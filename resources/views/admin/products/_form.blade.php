<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-warning">
                    <h2 class="title icon-pencil-alt-before">Detalhes</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            {!! Form::label('title', 'Título', ['class' => 'form-label']) !!}
                            {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}

                            @if($errors->has('title'))
                                <span class="help-block -error">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('subtitle', 'Subtitulo', ['class' => 'form-label']) !!}
                            {!! Form::text('subtitle', old('subtitle'), ['class' => 'form-control']) !!}

                            @if($errors->has('subtitle'))
                                <span class="help-block -error">{{ $errors->first('subtitle') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-6 col-md-3">
                            {!! Form::label('price', 'Preço', ['class' => 'form-label', 'data-placement' => 'top', 'title' => 'Preço que será comercializado.']) !!}
                            <div>
                                <money name="price" value="{{ isset($result) ? $result->masked_price : old('price') }}"
                                       class="form-control"
                                       v-bind="{decimal: ',', thousands: '.', prefix: 'R$', precision: 2}"/>
                            </div>

                            @if($errors->has('price'))
                                <span class="help-block -error">{{ $errors->first('price') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-3">
                            {!! Form::label('stock', 'Estoque', ['class' => 'form-label']) !!}
                            {!! Form::text('stock', old('stock'), ['class' => 'form-control']) !!}

                            @if($errors->has('stock'))
                                <span class="help-block -error">{{ $errors->first('stock') }}</span>
                            @endif
                        </div>

{{--                        <div class="form-group col-sm-3">
                            {!! Form::label('category_id', 'Categoria', ['class' => 'form-label']) !!}
                            {!! Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control custom-select', 'placeholder' => 'Selecione']) !!}

                            @if($errors->has('category_id'))
                                <span class="help-block -error">{{ $errors->first('category_id') }}</span>
                            @endif
                        </div>
--}}

                        <div class="form-group col-sm-12">
                            {!! Form::label('body', 'Descrição', ['class' => 'form-label']) !!}
                            {!! Form::textarea('body', old('body'), ['class' => 'form-control']) !!}

                            @if($errors->has('body'))
                                <span class="help-block -error">{{ $errors->first('body') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
