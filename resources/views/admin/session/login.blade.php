@extends('admin.layouts.public')

@section('content')

<section class="section -auth" style="background-image: none; background-color: #e9ecef;">
  <div class="container h-100">
    <div class="row justify-content-center h-100">
      <div class="col-10 col-sm-8 col-md-6 col-lg-4 align-self-center">
        <header class="section-heading">
          <div class="brand">
            <img src="{{ asset('/brzadmin/images/logo.jpeg') }}" alt="Make the Next Technology" width="100">
          </div>
        </header>
        <article class="section-content">
          @include('admin.session._form_login')
        </article>
      </div>
    </div>
  </div>
</section>
@stop

