@extends('admin.layouts.public')

@section('content')
@if (session('resent'))
<div class="alert alert-success alert-dashboard rounded-0" role="alert">
Um novo link de verificação foi enviado para o seu endereço de e-mail.
</div>
@endif

<section class="section -auth">
  <div class="container h-100">
    <div class="row justify-content-center h-100">
      <div class="col-10 col-sm-8 col-md-6 col-lg-4 align-self-center">
        <header class="section-heading">
          <div class="brand">
            <img src="{{ asset('/brzadmin/images/brand-vertical.svg') }}" alt="BRZ Admin">
          </div>
        </header>
        <article class="section-content">
          <div class="text-center">
            <p class="text-muted my-5 px-4">Seu endereço de e-mail não foi verificado, procure em sua caixa de entrada pelo e-mail de verificação ou solicite outro e-mail clicando abaixo:</p>
            <a class="btn btn-rounded btn-block btn-success font-weight-bold" title="Solicitar verificação" href="{{ route('admin.verification.resend') }}">Solicitar verificação</a>
          </div>
        </article>
      </div>
    </div>
  </div>
</section>
@stop
