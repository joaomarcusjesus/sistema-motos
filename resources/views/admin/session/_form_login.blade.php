<!-- form -->
{!! Form::open(['route' => 'admin.session.login', 'method' => 'POST', 'class' => 'form', 'role' => 'form']) !!}

  <div class="form-group">
    {!! Form::label('email', 'E-mail', ['class' => 'sr-only']) !!}
    {!! Form::email('email', null, ['class' => 'form-control form-rounded input', 'placeholder' => 'Seu e-mail']) !!}

    @if($errors->has('email'))
    <span class="help-block -error">{{ $errors->first('email') }}</span>
    @endif
  </div>

  <div class="form-group">
    {!! Form::label('password', 'Senha', ['class' => 'sr-only']) !!}
    {!! Form::password('password', ['class' => 'form-control form-rounded input', 'placeholder' => 'Sua senha']) !!}

    @if($errors->has('password'))
    <span class="help-block -error">{{ $errors->first('password') }}</span>
    @endif
  </div>

  <div class="form-actions text-center">
    <a href="{{ route('admin.password.forgot') }}" class="d-block my-4" title="Esqueceu a senha?">Esqueceu a senha?</a>
    {!! Form::button('Acessar', ['class' => 'btn btn-rounded btn-block btn-info  font-weight-bold', 'type' => 'submit']) !!}
  </div>

{!! Form::close() !!}
