<!-- menu -->
<div class="menu justify-content-md-end">
  <ul class="navbar-nav font-weight-bold">
    <li class="nav-item">
      <a href="{{ route('admin.dashboard.index') }}" class="nav-link {{ active('admin.dashboard.index') }}" title="Dashboard">Dashboard</a>
    </li>

    @can('view_audits')
    <li class="nav-item">
      <a href="{{ route('admin.audits.index') }}" class="nav-link {{ active('admin.audits.*') }}" title="Auditoria">Auditoria</a>
    </li>
    @endcan

    <li class="nav-item -avatar">
      <a href="{{ route('admin.account.show') }}" class="avatar" title="{{ auth()->user()->full_name }}">
        <img src="{{ auth()->user()->photo }}" alt="{{ auth()->user()->full_name }}" class="photo rounded-circle">
        <span class="text-hidden">{{ auth()->user()->full_name }}</span>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('home.index') }}" class="nav-link -with-icon icon-external-link-before" title="Ir para o site" target="_blank">
        <span class="text-hidden">Ir para o site</span>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('admin.session.logout') }}" class="nav-link -logout -with-icon icon-power-off-before" title="Sair">
        <span class="text-hidden">Sair</span>
      </a>
    </li>
  </ul>
</div>
