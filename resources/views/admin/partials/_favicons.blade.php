<!-- favicons -->
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/brzadmin/favicons/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" href="{{ asset('/brzadmin/favicons/favicon-32x32.png') }}" sizes="32x32">
<link rel="icon" type="image/png" href="{{ asset('/brzadmin/favicons/favicon-16x16.png') }}" sizes="16x16">
<link rel="manifest" href="{{ asset('/brzadmin/favicons/manifest.json') }}">
<link rel="mask-icon" href="{{ asset('/brzadmin/favicons/safari-pinned-tab.svg') }}" color="#3ec61c">
<link rel="shortcut icon" href="{{ asset('/brzadmin/favicons/favicon.ico') }}">
<meta name="apple-mobile-web-app-title" content="BRZ Admin">
<meta name="application-name" content="BRZ Admin">
<meta name="msapplication-config" content="{{ asset('/brzadmin/favicons/browserconfig.xml') }}">
<meta name="theme-color" content="#ffffff">
