<div class="card card-default -shadow mb-4">
  <header class="card-header bg-info">
    <h2 class="title icon-hashtag-before">Otimização para buscadores</h2>
    {!! Form::hidden('seo[id]', (isset($result->seo)) ? $result->seo->id : null) !!}
  </header>
  <div class="card-body">
    <div class="row">
      <div class="form-group col-sm-4">
        {!! Form::label('seo[meta_title]', 'Título da página', ['class' => 'form-label']) !!}
        {!! Form::text('seo[meta_title]', (isset($result->seo)) ? $result->seo->meta_title : old('seo.meta_title'), ['class' => 'form-control']) !!}

        @if($errors->has('seo.meta_title'))
        <span class="help-block -error">{{ $errors->first('seo.meta_title') }}</span>
        @endif
      </div>
      <div class="form-group col-sm-8">
        {!! Form::label('seo[meta_description]', 'Descrição da página', ['class' => 'form-label']) !!}
        {!! Form::text('seo[meta_description]', (isset($result->seo)) ? $result->seo->meta_description : old('seo.meta_description'), ['class' => 'form-control']) !!}

        @if($errors->has('seo.meta_description'))
        <span class="help-block -error">{{ $errors->first('seo.meta_description') }}</span>
        @endif
      </div>
      <div class="form-group col-sm-4">
        {!! Form::label('seo[image_facebook]', 'Capa Facebook (1200x630)', ['class' => 'form-label']) !!}
        {!! Form::file('seo[image_facebook]', ['class' => 'js-file-input']) !!}

        @if($errors->has('seo.image_facebook'))
        <span class="help-block -error">{{ $errors->first('seo.image_facebook') }}</span>
        @endif
      </div>
    </div>
  </div>
</div>
