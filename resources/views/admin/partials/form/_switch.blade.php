<div class="switch">
  {!! Form::hidden($name, 0) !!}
  {!! Form::checkbox($name, true, $value, array_merge(['class' => 'switch-input', 'id' => $name], $attributes)) !!}
  {!! Form::label($name, $label, ['class' => 'switch-label']) !!}
</div>
