<div class="card card-default -shadow mb-4">
  <header class="card-header bg-info">
    <h2 class="title icon-video-before">Vídeo</h2>
    {!! Form::hidden('video[id]', (isset($result->video)) ? $result->video->id : null) !!}
  </header>
  <div class="card-body">
    <div class="row">
      <div class="form-group col-sm-12">
        {!! Form::label('video[name]', 'Legenda', ['class' => 'form-label']) !!}
        {!! Form::text('video[name]', (isset($result->video)) ? $result->video->name : old('video.name'), ['class' => 'form-control']) !!}

        @if($errors->has('video.name'))
        <span class="help-block -error">{{ $errors->first('video.name') }}</span>
        @endif
      </div>
      <div class="form-group col-sm-12">
        {!! Form::label('video[reference]', 'Youtube ID', ['class' => 'form-label']) !!}
        {!! Form::text('video[reference]', (isset($result->video)) ? $result->video->reference : old('video.reference'), ['class' => 'form-control']) !!}

        @if($errors->has('video.reference'))
        <span class="help-block -error">{{ $errors->first('video.reference') }}</span>
        @endif
      </div>
      @if(isset($showImgInput))
      <div class="form-group col-sm-12">
        {!! Form::label('video[image]', 'Capa', ['class' => 'form-label js-tooltip', 'data-placement' => 'top', 'title' => '640x480']) !!}
        {!! Form::file('video[image]', ['class' => 'js-file-input']) !!}

        @if($errors->has('video.image'))
        <span class="help-block -error">{{ $errors->first('video.image') }}</span>
        @endif
      </div>
      @endif
    </div>
  </div>
</div>
