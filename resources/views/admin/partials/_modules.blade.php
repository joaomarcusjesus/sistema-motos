<!-- modules -->
<div class="collapse modules" id="modules" data-module="Modules">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        @foreach(config('brz.modules') as $module)
          @can($module['permission'])
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
              <a href="{{ route("admin.{$module['alias']}.index") }}" class="module {{ $module['icon'] }} {{ active(["admin/{$module['path']}", "admin/{$module['path']}/*"], '-active') }}" title="{{ $module['name'] }}">{{ $module['name'] }}</a>
            </div>
          @endcan
        @endforeach
      </div>
    </div>
  </div>
</div>
