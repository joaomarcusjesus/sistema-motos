<!-- header -->
<header class="header fixed-top">
  <nav class="navbar navbar-expand-lg navbar-dark" data-module="Menu">
    <button class="navbar-toggler toggle js-toggle-menu" type="button">
      <span class="bar"></span>
    </button>

    <div class="navbar-brand">
      <a href="{{ route('admin.dashboard.index') }}" title="Make the Next Technology" style="color: #FFFFFF;">
          Make the Next Technology
      </a>
    </div>

    <button class="btn btn-circle btn-modules js-get-modules icon-cog-before collapsed" type="button" data-toggle="collapse" data-target="#modules" aria-controls="modules" aria-expanded="false" aria-label="Módulos">
      <span class="sr-only">Módulos</span>
    </button>

    @include('admin.partials._menu')
  </nav>

  @include('admin.partials._modules')
</header>
