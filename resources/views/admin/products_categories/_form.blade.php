<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-warning">
                    <h2 class="title icon-pencil-alt-before">Detalhes</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            {!! Form::label('title', 'Categoria', ['class' => 'form-label']) !!}
                            {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}

                            @if($errors->has('title'))
                                <span class="help-block -error">{{ $errors->first('title') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
