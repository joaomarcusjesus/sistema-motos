@extends('admin.layouts.app')

@section('content')
<section class="module">
    <header class="module-heading">
        <div class="module-heading-inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-5">
                        <div class="title-area">
                            <h2 class="title align-middle">Usuários</h2>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="actions text-right">
                            <div class="actions-content">
                                <a href="{{ route('admin.users.create') }}" class="btn btn-success icon-plus-before" title="Adicionar">
                                    <span>Adicionar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <aside class="util mb-4">
        <div class="container-fluid">
            <div class="card card-default -shadow">
                <div class="card-body -search">
                    @include('admin.users.partials._search')
                </div>
            </div>
        </div>
    </aside>

    <article class="module-content">
        <div class="container-fluid">
            <div class="card card-default -shadow">
                <div class="card-body p-0">
                    <table class="table table-striped table-bordered -list">
                        <thead class="table-thead">
                            <tr class="table-row">
                                <th class="table-col">
                                    <span class="text">Usuário</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">E-mail</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Grupo</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Último acesso</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Status</span>
                                </th>
                                <th class="table-col -actions">
                                    <span class="text">Ações</span>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="table-tbody">
                            @foreach($results as $result)
                            <tr class="table-row" id="result-{{ $result->id }}">
                                <td class="table-col" data-th="Usuário">
                                    <div class="preview">
                                        <div class="preview-figure">
                                            <div class="preview-image bg-cover" style="background-image: url('{{ $result->photo }}');"></div>
                                        </div>
                                        <div class="preview-caption d-flex">
                                            <span class="text align-self-center">{{ $result->full_name }}</span>
                                        </div>
                                    </div>
                                </td>
                                <td class="table-col" data-th="E-mail">
                                    <span class="text">{{ $result->email }}</span>
                                </td>
                                <td class="table-col" data-th="Grupo">
                                    @foreach($result->roles as $role)
                                    <a href="{{ route('admin.users.index', ['grupo' => $role->name]) }}" class="badge badge-info -status">{{ $role->details }}</a>
                                    @endforeach
                                </td>
                                <td class="table-col" data-th="Último acesso">
                                    <span class="text">{{ $result->last_login }}</span>
                                </td>
                                <td class="table-col" data-th="Status">
                                    {!! isActive($result->active) !!}
                                </td>
                                <td class="table-col -actions" data-th="Ações">
                                    @can('edit_users')
                                    <a href="{{ route('admin.users.edit', ['id' => $result->id]) }}" class="btn btn-link -success icon-pencil-before js-tooltip" data-placement="top" title="Editar">
                                        <span class="sr-only">Editar</span>
                                    </a>
                                    @endcan

                                    @if($result->active === true)
                                        @can('delete_users')
                                        <a href="{{ route('admin.users.deactivate', ['id' => $result->id]) }}" data-type="confirm" data-message="Deseja desativar o usuário {{ $result->first_name }}?" data-confirm="Confirmar" class="js-confirm btn btn-link -danger icon-ban-before js-tooltip" data-placement="top" title="Desativar">
                                            <span class="sr-only">Desativar</span>
                                        </a>
                                        @endcan
                                        @else
                                        @can('edit_users')
                                        <a href="{{ route('admin.users.activate', ['id' => $result->id]) }}" data-type="confirm" data-message="Deseja ativar o usuário {{ $result->first_name }}?" data-confirm="Confirmar" class="js-confirm btn btn-link -success icon-check-before js-tooltip" data-placement="top" title="Ativar">
                                            <span class="sr-only">Ativar</span>
                                        </a>
                                        @endcan
                                    @endif

                                    @can('edit_users')
                                        <a href="{{ route('admin.users.destroy', ['id' => $result->id]) }}"
                                           data-target="#result-{{ $result->id }}" data-type="delete"
                                           data-message="Deseja excluir o usuario {{ $result->full_name }}?"
                                           data-confirm="Excluir"
                                           class="js-confirm btn btn-link -danger icon-trash-alt-before js-tooltip"
                                           data-placement="top" title="Excluir">
                                            <span class="sr-only">Excluir</span>
                                        </a>
                                    @endcan
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>

    <footer class="module-footing">
        {{ $results->render('admin.partials._pagination') }}
    </footer>
</section>
@stop
