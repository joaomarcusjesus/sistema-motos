<!-- form -->
{!! Form::open(['route' => 'admin.activation.processCreate', 'method' => 'POST', 'class' => 'form', 'role' => 'form']) !!}
  <div class="form-group">
    {!! Form::label('password', 'Senha', ['class' => 'sr-only']) !!}
    {!! Form::password('password', ['class' => 'form-control form-rounded input', 'placeholder' => 'Sua senha']) !!}
    @if($errors->has('password'))
    <span class="help-block -error">{{ $errors->first('password') }}</span>
    @endif
  </div>
  <div class="form-group">
    {!! Form::label('password_confirmation', 'Confirmar Senha', ['class' => 'sr-only']) !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control form-rounded input', 'placeholder' => 'Confirmar sua senha']) !!}

    @if($errors->has('password_confirmation'))
    <span class="help-block -error">{{ $errors->first('password_confirmation') }}</span>
    @endif
  </div>
  <div class="form-actions text-center">
    {!! Form::button('Acessar', ['class' => 'btn btn-rounded btn-block btn-success font-weight-bold', 'type' => 'submit']) !!}
  </div>
{!! Form::close() !!}
