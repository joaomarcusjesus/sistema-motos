<!-- form -->
{!! Form::open(['route' => 'admin.password.processForgot', 'method' => 'POST', 'class' => 'form', 'role' => 'form']) !!}
  <div class="form-group">
    {!! Form::label('email', 'E-mail', ['class' => 'sr-only']) !!}
    {!! Form::email('email', null, ['class' => 'form-control form-rounded input', 'placeholder' => 'Seu e-mail']) !!}
    @if($errors->has('email'))
    <span class="help-block -error">{{ $errors->first('email') }}</span>
    @endif
  </div>
  <div class="form-actions text-center">
    {!! Form::button('Enviar', ['class' => 'btn btn-rounded btn-block btn-success font-weight-bold', 'type' => 'submit']) !!}
  </div>
{!! Form::close() !!}
