@extends('admin.layouts.public')

@section('content')
<section class="section -auth">
  <div class="container h-100">
    <div class="row justify-content-center h-100">
      <div class="col-10 col-sm-8 col-md-6 col-lg-4 align-self-center">
        <header class="section-heading">
          <div class="brand">
            <img src="{{ asset('/brzadmin/images/brand-vertical.svg') }}" alt="BRZ Admin">
          </div>
        </header>
        <article class="section-content">
          <div class="text-center">
            <p class="text-muted my-4">Informe sua nova senha abaixo</p>
          </div>
          @include('admin.passwords._form_reset')
        </article>
      </div>
    </div>
  </div>
</section>
@stop
