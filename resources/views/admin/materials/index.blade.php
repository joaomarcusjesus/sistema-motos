@extends('admin.layouts.app')

@section('content')
    <section class="module">
        <header class="module-heading">
            <div class="module-heading-inner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-5">
                            <div class="title-area">
                                <h2 class="title align-middle">Materiais</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <aside class="util mb-4">
            <div class="container-fluid">
                <div class="card card-default -shadow">
                    <div class="card-body -search">
                        @include('admin.materials.partials._search')
                    </div>
                </div>
            </div>
        </aside>

        <article class="module-content">
            <div class="container-fluid">
                <div class="card card-default -shadow">
                    <div class="card-body p-0">
                        <table class="table table-striped table-bordered -list">
                            <thead class="table-thead">
                            <tr class="table-row">
                                <th class="table-col">
                                    <span class="text">Cliente</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Produto</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Valor</span>
                                </th>
                                <th class="table-col">
                                    <span class="text">Data</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="table-tbody">
                                @foreach($results as $result)
                                    <tr class="table-row" id="result-{{ $result->id }}">
                                        <td class="table-col" data-th="Cliente">
                                            <span class="text">{{ $result->motorcycle->client->full_name }}</span>
                                        </td>
                                        <td class="table-col" data-th="Produto">
                                            <span class="text">{{ $result->product->title }}</span>
                                        </td>
                                        <td class="table-col" data-th="Valor">
                                            <span class="text">{{ $result->product->maskedPrice }}</span>
                                        </td>
                                        <td class="table-col" data-th="Data">
                                            <span class="text">{{ $result->created_at->format('d/m/yy H:i') }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </article>

        <footer class="module-footing">
            {{ $results->render('admin.partials._pagination') }}
        </footer>
    </section>
@stop
