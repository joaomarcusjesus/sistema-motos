<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-warning">
                    <h2 class="title icon-pencil-alt-before">Detalhes</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            {!! Form::label('client_id', 'Cliente', ['class' => 'form-label']) !!}
                            {!! Form::select('client_id', $clients, old('client_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}

                            @if($errors->has('client_id'))
                                <span class="help-block -error">{{ $errors->first('client_id') }}</span>
                            @endif
                        </div>
                        <div class="form-group col-sm-4">
                            {!! Form::label('product_id', 'Produto', ['class' => 'form-label']) !!}
                            {!! Form::select('product_id', $products, old('product_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}

                            @if($errors->has('product_id'))
                                <span class="help-block -error">{{ $errors->first('product_id') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
