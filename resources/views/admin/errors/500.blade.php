@extends('admin.layouts.error')

@section('content')
@include('admin.errors.partials._content', ['errorStatus' => 500, 'errorMessage' => 'Ops, ocorreu um erro'])
@stop
