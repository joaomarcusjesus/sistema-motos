@extends('admin.layouts.error')

@section('content')
@include('admin.errors.partials._content', ['errorStatus' => 404, 'errorMessage' => 'Ops, página não encontrada'])
@stop
