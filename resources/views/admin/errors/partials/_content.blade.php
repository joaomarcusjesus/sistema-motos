<section class="section -error">
    <div class="container h-100">
        <div class="row justify-content-center h-100">
            <div class="col-10 col-sm-8 col-md-6 col-lg-4 align-self-center">
                <header class="section-heading">
                    <div class="brand">
                        <img src="{{ asset('/brzadmin/images/brand-vertical-inverse.svg') }}" alt="BRZ Admin">
                    </div>
                </header>
                <article class="section-content">
                    <h2 class="title">{!! $errorMessage !!}</h2>
                    <p class="status-code">{{ $errorStatus }}</p>
                    <a href="{{ route('admin.dashboard.index') }}" class="btn btn-success font-weight-bold" title="Voltar">Voltar</a>
                </article>
            </div>
        </div>
    </div>
</section>
