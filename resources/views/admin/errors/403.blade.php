@extends('admin.layouts.error')

@section('content')
@include('admin.errors.partials._content', ['errorStatus' => 403, 'errorMessage' => 'Ops, acesso negado'])
@stop
