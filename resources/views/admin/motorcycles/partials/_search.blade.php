<div class="search">
    {{ Form::open(['route' => 'admin.motorcycles.index', 'class' => 'form -default', 'method' => 'GET']) }}
    <div class="row">
        <div class="form-group col-sm-12 col-md-3 col-lg-3">
            {{ Form::label('nome', 'Nome', ['class' => 'sr-only']) }}
            {{ Form::text('nome', request('nome'), ['class' => 'form-control', 'placeholder' => 'Fabricante']) }}
        </div>

        <div class="form-group col-sm-4 col-md-3 col-lg-2">
            {{ Form::label('modelo', 'Modelo', ['class' => 'sr-only']) }}
            {{ Form::text('modelo', request('modelo'), ['class' => 'form-control', 'placeholder' => 'Modelo']) }}
        </div>

        <div class="form-group col-sm-4 col-md-3 col-lg-2">
            {{ Form::label('placa', 'Placa', ['class' => 'sr-only']) }}
            {{ Form::text('placa', request('placa'), ['class' => 'form-control', 'placeholder' => 'Placa']) }}
        </div>

        <div class="form-group col-sm-12 col-md-3 col-lg-3">
            {!! Form::label('cliente', 'Moto', ['class' => 'sr-only']) !!}
            {!! Form::select('cliente', $clients, request('cliente'), ['class' => 'form-control custom-select', 'placeholder' => 'Cliente']) !!}

            @if($errors->has('cliente'))
                <span class="help-block -error">{{ $errors->first('cliente') }}</span>
            @endif
        </div>

        <div class="form-group col-6 col-sm-2 col-md-2 col-lg-1">
            <button type="reset" class="btn btn-danger btn-block icon-times-before">
                <span class="sr-only">Resetar</span>
            </button>
        </div>

        <div class="form-group col-6 col-sm-2 col-md-2 col-lg-1">
            <button type="submit" class="btn btn-success btn-block icon-search-before">
                <span class="sr-only">Pesquisar</span>
            </button>
        </div>
    </div>
    {{ Form::close() }}
</div>
