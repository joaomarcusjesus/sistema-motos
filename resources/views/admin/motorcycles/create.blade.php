@extends('admin.layouts.app')

@section('content')
    <section class="module">
        {!! Form::open(['route' => ['admin.motorcycles.store'], 'class' => 'form -default', 'files' => true]) !!}
        <header class="module-heading">
            <div class="module-heading-inner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-3 col-sm-4">
                            <div class="title-area">
                                <h2 class="title align-middle">Nova Moto</h2>
                            </div>
                        </div>
                        <div class="col-9 col-sm-8">
                            <div class="actions text-right">
                                <div class="actions-content">
                                    <div class="d-inline-block mr-3">
                                        {{ Form::switch('active', null, 'Ativo?') }}
                                    </div>
                                    <a href="{{ route('admin.motorcycles.index') }}"
                                       class="btn btn-danger icon-chevron-left-before" title="Voltar">
                                        <span>Voltar</span>
                                    </a>
                                    <button type="submit" class="btn btn-success icon-save-before" title="Salvar">
                                        <span>Salvar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <article class="module-content">
            @include('admin.motorcycles._form')
        </article>
        {!! Form::close() !!}
    </section>
@stop
