<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-warning">
                    <h2 class="title icon-pencil-alt-before">Detalhes</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            {!! Form::label('title', 'Fabricante', ['class' => 'form-label']) !!}
                            {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}

                            @if($errors->has('title'))
                                <span class="help-block -error">{{ $errors->first('title') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('client_id', 'Cliente', ['class' => 'form-label']) !!}
                            {!! Form::select('client_id', $clients, old('client_id'), ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}

                            @if($errors->has('client_id'))
                                <span class="help-block -error">{{ $errors->first('client_id') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('color', 'Cor', ['class' => 'form-label']) !!}
                            {!! Form::text('color', old('color'), ['class' => 'form-control']) !!}

                            @if($errors->has('color'))
                                <span class="help-block -error">{{ $errors->first('color') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('model', 'Modelo', ['class' => 'form-label']) !!}
                            {!! Form::text('model', old('model'), ['class' => 'form-control']) !!}

                            @if($errors->has('model'))
                                <span class="help-block -error">{{ $errors->first('model') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('board', 'Placa', ['class' => 'form-label']) !!}
                            {!! Form::text('board', old('board'), ['class' => 'form-control']) !!}

                            @if($errors->has('board'))
                                <span class="help-block -error">{{ $errors->first('board') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('year', 'Ano', ['class' => 'form-label']) !!}
                            {!! Form::text('year', old('year'), ['class' => 'form-control']) !!}

                            @if($errors->has('year'))
                                <span class="help-block -error">{{ $errors->first('year') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('displacement', 'Cilindradas', ['class' => 'form-label']) !!}
                            {!! Form::text('displacement', old('displacement'), ['class' => 'form-control']) !!}

                            @if($errors->has('displacement'))
                                <span class="help-block -error">{{ $errors->first('displacement') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-12">
                            {!! Form::label('body', 'Obervações', ['class' => 'form-label']) !!}
                            {!! Form::textarea('body', old('body'), ['class' => 'form-control']) !!}

                            @if($errors->has('body'))
                                <span class="help-block -error">{{ $errors->first('body') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
