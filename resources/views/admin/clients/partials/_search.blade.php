<div class="search">
    {{ Form::open(['route' => 'admin.clients.index', 'class' => 'form -default', 'method' => 'GET']) }}
    <div class="row">
        <div class="form-group col-sm-12 col-md-3 col-lg-6">
            {{ Form::label('nome', 'Nome', ['class' => 'sr-only']) }}
            {{ Form::text('nome', request('nome'), ['class' => 'form-control', 'placeholder' => 'Nome']) }}
        </div>

        <div class="form-group col-sm-4 col-md-3 col-lg-4">
            {{ Form::label('email', 'E-mail', ['class' => 'sr-only']) }}
            {{ Form::text('email', request('email'), ['class' => 'form-control', 'placeholder' => 'E-mail']) }}
        </div>

        <div class="form-group col-6 col-sm-2 col-md-2 col-lg-1">
            <button type="reset" class="btn btn-danger btn-block icon-times-before">
                <span class="sr-only">Resetar</span>
            </button>
        </div>

        <div class="form-group col-6 col-sm-2 col-md-2 col-lg-1">
            <button type="submit" class="btn btn-success btn-block icon-search-before">
                <span class="sr-only">Pesquisar</span>
            </button>
        </div>
    </div>
    {{ Form::close() }}
</div>
