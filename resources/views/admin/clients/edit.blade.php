@extends('admin.layouts.app')

@section('content')
    <section class="module">
        {!! Form::model($result, ['method' => 'PUT', 'route' => ['admin.clients.update',  'id' => $result->id], 'class' => 'form -default', 'files' => true]) !!}
        <header class="module-heading">
            <div class="module-heading-inner">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-3 col-sm-4">
                            <div class="title-area">
                                <h2 class="title align-middle">Editar Cliente</h2>
                            </div>
                        </div>
                        <div class="col-9 col-sm-8">
                            <div class="actions text-right">
                                <div class="actions-content">
                                    <div class="d-inline-block mr-3">
                                        {{ Form::switch('active', null, 'Ativo?') }}
                                    </div>
                                    <a href="{{ route('admin.clients.index') }}"
                                       class="btn btn-danger icon-chevron-left-before" title="Voltar">
                                        <span>Voltar</span>
                                    </a>
                                    <button type="submit" class="btn btn-success icon-save-before" title="Atualizar">
                                        <span>Atualizar</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <article class="module-content">
            @include('admin.clients._form')
        </article>
        {!! Form::close() !!}
    </section>
@stop
