<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card card-default -shadow mb-4">
                <header class="card-header bg-warning">
                    <h2 class="title icon-pencil-alt-before">Detalhes</h2>
                </header>
                <div class="card-body">
                    <div class="row">
                        <div class="form-group col-sm-4">
                            {!! Form::label('first_name', 'Nome', ['class' => 'form-label']) !!}
                            {!! Form::text('first_name', old('first_name'), ['class' => 'form-control']) !!}

                            @if($errors->has('first_name'))
                                <span class="help-block -error">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('last_name', 'Sobrenome', ['class' => 'form-label']) !!}
                            {!! Form::text('last_name', old('last_name'), ['class' => 'form-control']) !!}

                            @if($errors->has('last_name'))
                                <span class="help-block -error">{{ $errors->first('last_name') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('phone', 'Telefone', ['class' => 'form-label']) !!}
                            <div>
                                <the-mask name="phone" value="{{ isset($result) ? $result->phone : old('phone') }}"
                                          class="form-control" type="tel"
                                          :mask="['(##) ####-####', '(##) #####-####']"/>
                            </div>

                            @if($errors->has('phone'))
                                <span class="help-block -error">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-4">
                            {!! Form::label('email', 'Email', ['class' => 'form-label']) !!}
                            {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}

                            @if($errors->has('email'))
                                <span class="help-block -error">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group col-sm-8">
                            {!! Form::label('address', 'Endereço', ['class' => 'form-label']) !!}
                            {!! Form::text('address', old('address'), ['class' => 'form-control']) !!}

                            @if($errors->has('address'))
                                <span class="help-block -error">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
