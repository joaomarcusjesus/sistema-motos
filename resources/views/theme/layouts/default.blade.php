<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {!! SEO::generate() !!}

        @include('theme.util._favicons')
        @include('theme.util._styles')
    </head>
    <body class="body -{{ controller_name(null, false) }}">
        @include('theme.header._index')

        <main class="main" role="main">
            @yield('content')
        </main>

        @include('theme.footer._index')

        @include('theme.util._scripts')
        @include('theme.util._google_analytics')
    </body>
</html>
