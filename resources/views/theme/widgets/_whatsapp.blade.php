<div class="chat-widget">
    @php($phone = '5583')
    @php($whatsapp = (Agent::isMobile() || Agent::isTablet()) ? "whatsapp://send?phone=${$phone}&abid=${$phone}" : "https://api.whatsapp.com/send?phone=${$phone}&abid=${$phone}")
    <a href="{{ $whatsapp }}" target="_blank" class="link -chat icon-chat-before" title="WhatsApp">
        <span class="sr-only">WhatsApp</span>
    </a>
</div>
