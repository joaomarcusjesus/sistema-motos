<ul class="list-inline">
    @if(config('settings.facebook'))
    <li class="list-inline-item">
        <a href="{{ config('settings.facebook') }}" target="_blank" class="link -social icon-facebook-before" title="Facebook">
            <span class="sr-only">Facebook</span>
        </a>
    </li>
    @endif

    @if(config('settings.instagram'))
    <li class="list-inline-item">
        <a href="{{ config('settings.instagram') }}" target="_blank" class="link -social icon-instagram-before" title="Instagram">
            <span class="sr-only">Instagram</span>
        </a>
    </li>
    @endif

    @if(config('settings.linkedin'))
    <li class="list-inline-item">
        <a href="{{ config('settings.linkedin') }}" target="_blank" class="link -social icon-linkedin-before" title="LinkedIn">
            <span class="sr-only">LinkedIn</span>
        </a>
    </li>
    @endif
</ul>
