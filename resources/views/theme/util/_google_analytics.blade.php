@if (config('app.env') == 'production' && config('settings.google_analytics') !== null)
<!-- analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{ config('settings.google_analytics') }}"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', '{{ config('settings.google_analytics') }}');
</script>
@endif
